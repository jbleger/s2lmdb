from functools import cached_property
from dataclasses import dataclass
from typing import FrozenSet, Tuple


@dataclass(frozen=True)
class CitRef:
    influential: FrozenSet[int]
    noninfluential: FrozenSet[int]

    @cached_property
    def all(self) -> FrozenSet:
        return self.influential | self.noninfluential


@dataclass(frozen=True)
class Paper:
    corpusid: int
    sha: str
    title: str
    journal: str
    abstract: str
    tldr: str
    publicationdate: str
    isopenaccess: True
    authors: Tuple[int]
    citations: CitRef
    references: CitRef

    @cached_property
    def ncitations(self):
        return len(self.citations.all)

    @cached_property
    def nreferences(self):
        return len(self.references.all)

@dataclass(frozen=True)
class Author:
    authorid: int
    name: str
    papers: FrozenSet[int]

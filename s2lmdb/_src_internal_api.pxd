#cython: language_level=3

from libc.stdint cimport *

cdef extern from "<lmdb.h>":
    char* mdb_strerror(int)
    cdef struct MDB_cursor:
        pass
    unsigned int MDB_FIRST
    unsigned int MDB_NEXT
    unsigned int MDB_NOTFOUND


cdef extern from "src/data.h":
    ctypedef uint32_t corpusid_t
    ctypedef uint32_t authorid_t

    cdef struct paper_fixed_data_t:
        uint32_t titlelen
        uint32_t journallen
        uint16_t authorslen
        uint16_t pubyear
        uint8_t pubmonth
        uint8_t pubday

    cdef struct paper_t:
        paper_fixed_data_t* fixed
        char* title
        char* journal
        authorid_t* authors

    cdef struct sha1_t:
        pass

    cdef struct abstract_t:
        ssize_t abstractlen
        char* abstract

    cdef struct tldr_t:
        ssize_t tldrlen
        char* tldr

    cdef struct cpaper_fixed_data_t:
        sha1_t sha1
        uint32_t titlelen
        uint32_t journallen
        uint16_t authorslen
        uint32_t abstractlen
        uint32_t tldrlen
        uint32_t influential_citations_len
        uint32_t noninfluential_citations_len
        uint32_t influential_references_len
        uint32_t noninfluential_references_len
        uint16_t pubyear
        uint8_t pubmonth
        uint8_t pubday
        uint8_t isopenaccess

    cdef struct cpaper_t:
        cpaper_fixed_data_t* fixed
        char* title
        char* journal
        authorid_t* authors
        char* abstract
        char* tldr
        corpusid_t* all_citeref
        corpusid_t* influential_citations
        corpusid_t* noninfluential_citations
        corpusid_t* influential_references
        corpusid_t* noninfluential_references

    cdef struct cauthor_fixed_data_t:
        uint32_t namelen
        uint32_t paperslen

    cdef struct cauthor_t:
        cauthor_fixed_data_t* fixed
        char* name
        corpusid_t* papers



cdef extern from "src/db.h":
    cdef struct db_t:
        pass # I do not want cython code interact directly with the content of this struct

cdef extern from "src/cdb.h":
    cdef struct cdb_t:
        pass # I do not want cython code interact directly with the content of this struct

    ctypedef MDB_cursor* cdb_papers_cursor_t
    ctypedef MDB_cursor* cdb_authors_cursor_t

cdef extern from "src/db_interact.c":
    int db_write_paper(db_t* db,
                   corpusid_t corpusid,
                   char* title,
                   uint32_t titlelen,
                   char* journal,
                   uint32_t journallen,
                   authorid_t* authors,
                   uint16_t authorslen,
                   uint16_t pubyear,
                   uint8_t pubmonth,
                   uint8_t pubday,
                   uint8_t isopenaccess)

    int db_write_paper_id(db_t* db, corpusid_t corpusid, sha1_t* sha1, int primary)

    int db_write_abstract(db_t* db, corpusid_t corpusid, char* abstract, ssize_t abstractlen)

    int db_write_tldr(db_t* db, corpusid_t corpusid, char* tldr, ssize_t tldrlen)

    int db_write_author(db_t* db, authorid_t authorid, char* author, ssize_t authorlen)

    int db_write_citation(db_t* db, corpusid_t citingcorpusid, corpusid_t citedcorpusid, int influential);


cdef extern from "src/db.c":
    int db_open_database(db_t* db, char* pathname, uint64_t ramGB, int rw)
    int db_commit_database(db_t* db)
    void db_close_database(db_t* db)

cdef extern from "src/cdb.c":
    int cdb_open_database(cdb_t* db, char* pathname, int rw)
    int cdb_commit_database(cdb_t* db)
    void cdb_close_database(cdb_t* db)

cdef extern from "src/data_manip.c":
    pass

cdef extern from "src/cdb_convert.c":
    int cdb_db_convert(cdb_t* cdb, db_t* db)

cdef extern from "src/cdb_query.c":
    int cdb_get_author(cdb_t* cdb, authorid_t authorid, cauthor_t* cauthor) nogil
    int cdb_get_authorslen(cdb_t* cdb, uint64_t *authorslen)
    int cdb_create_authors_cursor(cdb_t* cdb, cdb_authors_cursor_t* cursor)
    int cdb_authors_cursor_get(cdb_authors_cursor_t* cursor, authorid_t* authorid, cauthor_t* cauthor, unsigned int what)
    void cdb_close_authors_cursor(cdb_papers_cursor_t* cursor)
    int cdb_get_paper(cdb_t* db, corpusid_t corpusid, cpaper_t* cpaper) nogil
    int cdb_get_corpusid_from_sha1(cdb_t* cdb, sha1_t* sha1, corpusid_t *corpusid)
    int cdb_get_paperslen(cdb_t* cdb, uint64_t *paperslen)
    int cdb_create_papers_cursor(cdb_t* cdb, cdb_papers_cursor_t* cursor)
    void cdb_close_papers_cursor(cdb_papers_cursor_t* cursor)
    int cdb_papers_cursor_get(cdb_papers_cursor_t* cursor, corpusid_t* corpusid, cpaper_t* cpaper, unsigned int what)

cdef extern from "src/db_merge.c":
    int db_merge_all_until_one(db_t *db)

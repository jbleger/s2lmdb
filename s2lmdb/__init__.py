_version_info = (0, 0, 1)
__version__ = ".".join(f"{x}" for x in _version_info)

from ._internals import WorkDB
from ._query import S2database

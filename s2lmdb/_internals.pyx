
#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False

from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
from libc.stdlib cimport malloc, free
from cython.parallel import prange, parallel

from s2lmdb._src_internal_api cimport *

import zlib


cdef default_strencoder(x):
    if x:
        return zlib.compress(x.encode('utf8'))
    return b''


cdef default_strdecoder(char* x, Py_ssize_t length, int defaultnone = False):
    if length:
        return zlib.decompress(x[:length]).decode('utf8')
    if defaultnone:
        return None
    return ''

from ._data import CitRef, Paper, Author

cdef cpaper_to_paperobj(corpusid_t corpusid, cpaper_t* cpaper):
    citations = CitRef(
        influential=frozenset([
            int(x) for x in cpaper.influential_citations[:cpaper.fixed.influential_citations_len]
        ]),
        noninfluential=frozenset([
            int(x) for x in cpaper.noninfluential_citations[:cpaper.fixed.noninfluential_citations_len]
        ]),
    )
    references = CitRef(
        influential=frozenset([
            int(x) for x in cpaper.influential_references[:cpaper.fixed.influential_references_len]
        ]),
        noninfluential=frozenset([
            int(x) for x in cpaper.noninfluential_references[:cpaper.fixed.noninfluential_references_len]
        ]),
    )
    authors = tuple([int(x) for x in cpaper.authors[: cpaper.fixed.authorslen]])
    sha = ''.join([f'{x:02x}' for x in (<char*>&cpaper.fixed.sha1)[:20]])

    publicationdate = (
        f"{cpaper.fixed.pubyear:04d}-{cpaper.fixed.pubmonth:02d}-{cpaper.fixed.pubday:02d}"
        if cpaper.fixed.pubyear
        else None
    )
    return Paper(
        corpusid=corpusid,
        sha=sha,
        title=default_strdecoder(cpaper.title, cpaper.fixed.titlelen, True),
        journal=default_strdecoder(cpaper.journal, cpaper.fixed.journallen, True),
        abstract=default_strdecoder(cpaper.abstract, cpaper.fixed.abstractlen, True),
        tldr=default_strdecoder(cpaper.tldr, cpaper.fixed.tldrlen, True),
        publicationdate=publicationdate,
        isopenaccess=bool(cpaper.fixed.isopenaccess),
        authors=authors,
        citations=citations,
        references=references,
    )

cdef cauthor_to_authorobj(authorid_t authorid, cauthor_t* cauthor):
    return Author(
        authorid=authorid,
        name=default_strdecoder(cauthor.name, cauthor.fixed.namelen, True),
        papers=frozenset([int(x) for x in cauthor.papers[:cauthor.fixed.paperslen]]),
        )

class LMDBError(Exception):
    pass

cdef class DB:
    cdef cdb_t cdb
    cdef int closed

    def __cinit__(self, pathname, _writable=False):
        cdef int rc;
        encoded_pathname = pathname.encode("utf8")
        rc = cdb_open_database(&self.cdb, encoded_pathname, 0)
        if rc:
            self.closed = True
            raise LMDBError(mdb_strerror(rc))
        self.closed = False

    def close(self):
        if not self.closed:
            cdb_close_database(&self.cdb)
            self.closed = True

    def __dealloc__(self):
        self.close()

    def __repr__(self):
        return f"<DB at {hex(id(self))}, "+("closed" if self.closed else "opened")+">"

    def get_many_papers(self, keys_sequence, int raisekeyerror, default=None):
        if self.closed:
            raise LMDBError

        cdef Py_ssize_t i

        cdef Py_ssize_t lkeys = len(keys_sequence)

        cdef corpusid_t* keys = <corpusid_t*>malloc(lkeys*sizeof(corpusid_t))
        for i in range(lkeys):
            keys[i] = keys_sequence[i]
        cdef cpaper_t* cpaper = <cpaper_t*>malloc(lkeys*sizeof(cpaper_t))
        cdef int* rcs = <int*>malloc(lkeys*sizeof(int))

        for i in prange(lkeys, nogil=True, schedule='guided'):
            rcs[i] = cdb_get_paper(&self.cdb, keys[i], &cpaper[i])

        ret = list()
        for i in range(lkeys):
            if rcs[i]==MDB_NOTFOUND:

                if raisekeyerror:
                    free(cpaper)
                    free(keys)
                    free(rcs)
                    raise KeyError(f'key corpusid:{keys[i]!r} not found')
                else:
                    ret.append(default)
            elif rcs[i]:
                free(cpaper)
                free(keys)
                free(rcs)
                raise LMDBError(mdb_strerror(rcs[i]))
            else:
                ret.append(cpaper_to_paperobj(keys[i], &cpaper[i]))

        free(cpaper)
        free(keys)
        free(rcs)

        return ret

    def get_author_by_authorid(self, authorid_t authorid):
        if self.closed:
            raise LMDBError

        cdef int rc

        cdef cauthor_t cauthor
        rc = cdb_get_author(&self.cdb, authorid, &cauthor)
        if rc==MDB_NOTFOUND:
            raise KeyError(f'key authorid:{authorid!r} not found')
        if rc:
            raise LMDBError(mdb_strerror(rc))
        return cauthor_to_authorobj(authorid, &cauthor)

    def get_many_authors(self, authorid_sequence, int raisekeyerror, default=None):
        if self.closed:
            raise LMDBError

        cdef Py_ssize_t i
        cdef Py_ssize_t lauthors = len(authorid_sequence)

        cdef authorid_t* authorids = <authorid_t*>malloc(lauthors*sizeof(authorid_t))
        for i in range(lauthors):
            authorids[i] = authorid_sequence[i]
        cdef cauthor_t* cauthors = <cauthor_t*>malloc(lauthors*sizeof(cauthor_t))
        cdef int* rcs = <int*>malloc(lauthors*sizeof(int))

        for i in prange(lauthors, nogil=True, schedule='guided'):
            rcs[i] = cdb_get_author(&self.cdb, authorids[i], &cauthors[i])
        
        ret = list()
        for i in range(lauthors):
            if rcs[i]==MDB_NOTFOUND:
                if raisekeyerror:
                    free(cauthors)
                    free(authorids)
                    free(rcs)
                    raise KeyError(f'key authorid:{authorids[i]!r} not found')
                else:
                    ret.append(default)
            elif rcs[i]:
                free(cauthors)
                free(authorids)
                free(rcs)
                raise LMDBError(mdb_strerror(rcs[i]))
            else:
                ret.append(cauthor_to_authorobj(authorids[i], &cauthors[i]))
        
        free(cauthors)
        free(authorids)
        free(rcs)

        return ret


    def authorslen(self):
        if self.closed:
            raise LMDBError

        cdef int rc
        cdef uint64_t authorslen;
        rc = cdb_get_authorslen(&self.cdb, &authorslen)
        if rc:
            raise LMDBError(mdb_strerror(rc))
        return authorslen
    
    def get_all_authors(self):
        if self.closed:
            raise LMDBError

        cdef int rc
        cdef cdb_authors_cursor_t cursor
        cdef authorid_t authorid
        cdef cauthor_t cauthor
        rc = cdb_create_authors_cursor(&self.cdb, &cursor)
        if rc:
            raise LMDBError(mdb_strerror(rc))
        try:
            rc = cdb_authors_cursor_get(&cursor, &authorid, &cauthor, MDB_FIRST)
            if rc!=MDB_NOTFOUND:
                if rc:
                    raise LMDBError(mdb_strerror(rc))
                yield (int(authorid), cauthor_to_authorobj(authorid, &cauthor))
                while True:
                    rc = cdb_authors_cursor_get(&cursor, &authorid, &cauthor, MDB_NEXT)
                    if rc == MDB_NOTFOUND:
                        break
                    if rc:
                        raise LMDBError(mdb_strerror(rc))
                    yield (int(authorid), cauthor_to_authorobj(authorid, &cauthor))
        finally:
            cdb_close_authors_cursor(&cursor)

    def get_paper_by_corpusid(self, corpusid_t corpusid):
        if self.closed:
            raise LMDBError

        cdef int rc

        cdef cpaper_t cpaper
        rc = cdb_get_paper(&self.cdb, corpusid, &cpaper)
        if rc==MDB_NOTFOUND:
            raise KeyError(f'key corpusid:{corpusid!r} not found')
        if rc:
            raise LMDBError(mdb_strerror(rc))
        return cpaper_to_paperobj(corpusid, &cpaper)

    def get_paper_by_sha1(self, sha1):
        if self.closed:
            raise LMDBError

        cdef uint8_t[20] buf
        cdef corpusid_t corpusid
        cdef int rc
        buf = bytes(int(sha1[(2 * i) : 2 * (i + 1)], 16) for i in range(20))
        rc = cdb_get_corpusid_from_sha1(&self.cdb, <sha1_t*>&buf[0], &corpusid)
        if rc==MDB_NOTFOUND:
            raise KeyError(f'key sha1:{sha1!r} not found')
        if rc:
            raise LMDBError(mdb_strerror(rc))
        cdef cpaper_t cpaper
        rc = cdb_get_paper(&self.cdb, corpusid, &cpaper)
        if rc==MDB_NOTFOUND:
            raise KeyError(f'key corpusid:{corpusid!r} (from sha1:{sha1!r}) not found')
        if rc:
            raise LMDBError(mdb_strerror(rc))
        return cpaper_to_paperobj(corpusid, &cpaper)

    def paperslen(self):
        if self.closed:
            raise LMDBError

        cdef int rc
        cdef uint64_t paperslen;
        rc = cdb_get_paperslen(&self.cdb, &paperslen)
        if rc:
            raise LMDBError(mdb_strerror(rc))
        return paperslen

    def get_all_papers(self):
        if self.closed:
            raise LMDBError

        cdef int rc
        cdef cdb_papers_cursor_t cursor
        cdef corpusid_t corpusid
        cdef cpaper_t cpaper
        rc = cdb_create_papers_cursor(&self.cdb, &cursor)
        if rc:
            raise LMDBError(mdb_strerror(rc))
        try:
            rc = cdb_papers_cursor_get(&cursor, &corpusid, &cpaper, MDB_FIRST)
            if rc!=MDB_NOTFOUND:
                if rc:
                    raise LMDBError(mdb_strerror(rc))
                yield (int(corpusid), cpaper_to_paperobj(corpusid, &cpaper))
                while True:
                    rc = cdb_papers_cursor_get(&cursor, &corpusid, &cpaper, MDB_NEXT)
                    if rc == MDB_NOTFOUND:
                        break
                    if rc:
                        raise LMDBError(mdb_strerror(rc))
                    yield (int(corpusid), cpaper_to_paperobj(corpusid, &cpaper))
        finally:
            cdb_close_papers_cursor(&cursor)



cdef class WorkDB:
    cdef db_t db
    cdef int rw
    cdef int closed

    def __cinit__(self, pathname, ramGB=16, rw=False):
        cdef int rc;
        encoded_pathname = pathname.encode("utf8")
        rc = db_open_database(&self.db, encoded_pathname, ramGB, rw)
        if rc:
            self.closed = True
            raise LMDBError(mdb_strerror(rc))
        self.rw = rw
        self.closed = False

    def commit(self):
        cdef int rc
        if self.rw:
            rc = db_commit_database(&self.db)
            if rc:
                self.close()
                raise LMDBError(mdb_strerror(rc))

    def close(self):
        if not self.closed:
            db_close_database(&self.db)
            self.closed = True

    def __dealloc__(self):
        self.close()


    def insert_papers(self, iterable):
        if self.closed:
            raise LMDBError

        cdef authorid_t* authors
        cdef Py_ssize_t i
        for obj in iterable:
            title = default_strencoder(obj['title'])
            ojournal = obj['journal']
            journal = default_strencoder(
                    ojournal["name"]
                if ojournal is not None and ojournal["name"]
                else ""
            )
            if obj['authors'] is not None:
                py_authors = [int(x['authorId']) for x in obj['authors'] if x is not
                          None and x['authorId'] is not None]
            else:
                py_authors = []
            authors = <authorid_t*>malloc(sizeof(authorid_t)*len(py_authors))
            for i,a in enumerate(py_authors):
                authors[i] = a
            pubyear, pubmonth, pubday = (
                (int(x) for x in obj["publicationdate"].split("-"))
                if obj["publicationdate"] is not None
                else (0, 0, 0)
            )


            db_write_paper(
                &self.db,
                obj['corpusid'],
                title,
                len(title),
                journal,
                len(journal),
                authors,
                len(py_authors),
                pubyear,
                pubmonth,
                pubday,
                obj['isopenaccess'],
            )

            free(authors)

    def insert_paper_ids(self, iterable):
        if self.closed:
            raise LMDBError

        cdef uint8_t[20] buf

        for obj in iterable:
            v = obj['sha']
            buf = bytes(int(v[(2 * i) : 2 * (i + 1)], 16) for i in range(20))
            db_write_paper_id(
                &self.db,
                obj['corpusid'],
                <sha1_t*>&buf[0],
                obj['primary'],
            )

    def insert_abstracts(self, iterable):
        if self.closed:
            raise LMDBError

        for obj in iterable:
            abstract = default_strencoder(obj["abstract"])
            db_write_abstract(
                &self.db,
                obj["corpusid"],
                abstract,
                len(abstract),
            )

    def insert_tldrs(self, iterable):
        if self.closed:
            raise LMDBError

        for obj in iterable:
            tldr = default_strencoder(obj["text"])
            db_write_tldr(
                &self.db,
                obj["corpusid"],
                tldr,
                len(tldr),
            )

    def insert_authors(self, iterable):
        if self.closed:
            raise LMDBError

        for obj in iterable:
            if obj["authorid"] is None:
                continue

            author = default_strencoder(obj["name"])
            db_write_author(
                &self.db,
                int(obj["authorid"]),
                author,
                len(author),
            )

    def insert_citations(self, iterable):
        if self.closed:
            raise LMDBError

        for obj in iterable:
            if obj["citingcorpusid"] is not None and obj["citedcorpusid"] is not None:
                citingcorpusid = int(obj["citingcorpusid"])
                citedcorpusid = int(obj["citedcorpusid"])
                if citingcorpusid>=0 and citedcorpusid>=0:
                    db_write_citation(
                        &self.db,
                        int(obj["citingcorpusid"]),
                        int(obj["citedcorpusid"]),
                        bool(obj["isinfluential"]),
                    )

    def merge(self):
        rc = db_merge_all_until_one(&self.db)
        if rc:
            raise LMDBError(mdb_strerror(rc))

    def convert_to_DB(self, pathname):
        cdef int rc;
        cdef cdb_t cdb;
        encoded_pathname = pathname.encode('utf8')
        rc = cdb_open_database(&cdb, encoded_pathname, 1)
        if rc:
            raise LMDBError(mdb_strerror(rc))
        rc = cdb_db_convert(&cdb, &self.db)
        if rc:
            cdb_close_database(&cdb)
            raise LMDBError(mdb_strerror(rc))
        cdb_commit_database(&cdb)
        cdb_close_database(&cdb)

        return DB(pathname)

#include "data_manip.h"

void
data_corpusid_to_mdb_val(corpusid_t* corpusid, MDB_val* val)
{
    val->mv_size = sizeof(corpusid_t);
    val->mv_data = (void*)corpusid;
}

void
data_authorid_to_mdb_val(authorid_t* authorid, MDB_val* val)
{
    val->mv_size = sizeof(authorid_t);
    val->mv_data = (void*)authorid;
}

void
data_mdb_val_to_paper(MDB_val* data, paper_t* paper)
{
    paper->fixed = (paper_fixed_data_t*)data->mv_data;
    paper->title = ((char*)paper->fixed) + sizeof(paper_fixed_data_t);
    paper->journal = ((char*)paper->fixed) + sizeof(paper_fixed_data_t) +
                     paper->fixed->titlelen;
    paper->authors =
      (authorid_t*)(((char*)paper->fixed) + sizeof(paper_fixed_data_t) +
                    paper->fixed->titlelen + paper->fixed->journallen);
}

void
data_create_paper(paper_t* paper,
                  char* title,
                  uint32_t titlelen,
                  char* journal,
                  uint32_t journallen,
                  authorid_t* authors,
                  uint16_t authorslen,
                  uint16_t pubyear,
                  uint8_t pubmonth,
                  uint8_t pubday,
                  uint8_t isopenaccess)
{
    paper->fixed =
      (paper_fixed_data_t*)malloc(sizeof(paper_fixed_data_t) + titlelen +
                                  journallen + authorslen * sizeof(authorid_t));
    if (!paper->fixed)
        abort();
    paper->fixed->titlelen = titlelen;
    paper->fixed->journallen = journallen;
    paper->fixed->authorslen = authorslen;
    paper->fixed->pubyear = pubyear;
    paper->fixed->pubmonth = pubmonth;
    paper->fixed->pubday = pubday;
    paper->fixed->isopenaccess = isopenaccess;
    paper->title = ((char*)paper->fixed) + sizeof(paper_fixed_data_t);
    paper->journal =
      ((char*)paper->fixed) + sizeof(paper_fixed_data_t) + titlelen;
    paper->authors =
      (authorid_t*)(((char*)paper->fixed) + sizeof(paper_fixed_data_t) +
                    titlelen + journallen);
    memcpy(paper->title, title, titlelen);
    memcpy(paper->journal, journal, journallen);
    memcpy(paper->authors, authors, authorslen * sizeof(authorid_t));
}

void
data_free_paper(paper_t* paper)
{
    free(paper->fixed);
}

void
data_paper_to_mdb_val(paper_t* paper, MDB_val* data)
{
    data->mv_size = sizeof(paper_fixed_data_t) + paper->fixed->titlelen +
                    paper->fixed->journallen +
                    paper->fixed->authorslen * sizeof(authorid_t);
    data->mv_data = paper->fixed;
}

void
data_mdb_val_to_cpaper(MDB_val* data, cpaper_t* cpaper)
{
    cpaper->fixed = (cpaper_fixed_data_t*)data->mv_data;
    ssize_t s1 = sizeof(cpaper_fixed_data_t);
    ssize_t s2 = s1 + cpaper->fixed->titlelen;
    ssize_t s3 = s2 + cpaper->fixed->journallen;
    ssize_t s4 = s3 + cpaper->fixed->authorslen * sizeof(authorid_t);
    ssize_t s5 = s4 + cpaper->fixed->abstractlen;
    ssize_t s6 = s5 + cpaper->fixed->tldrlen;
    ssize_t s7 =
      s6 + cpaper->fixed->influential_citations_len * sizeof(corpusid_t);
    ssize_t s8 =
      s7 + cpaper->fixed->noninfluential_citations_len * sizeof(corpusid_t);
    ssize_t s9 =
      s8 + cpaper->fixed->influential_references_len * sizeof(corpusid_t);
    ssize_t s10 =
      s9 + cpaper->fixed->noninfluential_references_len * sizeof(corpusid_t);
    assert(s10 == data->mv_size);

    cpaper->title = (char*)cpaper->fixed + s1;
    cpaper->journal = (char*)cpaper->fixed + s2;
    cpaper->authors = (authorid_t*)((char*)cpaper->fixed + s3);
    cpaper->abstract = (char*)cpaper->fixed + s4;
    cpaper->tldr = (char*)cpaper->fixed + s5;
    cpaper->influential_citations = (corpusid_t*)((char*)cpaper->fixed + s6);
    cpaper->noninfluential_citations = (corpusid_t*)((char*)cpaper->fixed + s7);
    cpaper->influential_references = (corpusid_t*)((char*)cpaper->fixed + s8);
    cpaper->noninfluential_references =
      (corpusid_t*)((char*)cpaper->fixed + s9);

    cpaper->all_citeref = cpaper->influential_citations; // first in memory
}

void
data_create_cpaper(cpaper_t* cpaper,
                   paper_t* paper,
                   sha1_t* sha1,
                   char* abstract,
                   uint32_t abstractlen,
                   char* tldr,
                   uint32_t tldrlen,
                   corpusid_t* influential_citations,
                   uint32_t influential_citations_len,
                   corpusid_t* noninfluential_citations,
                   uint32_t noninfluential_citations_len,
                   corpusid_t* influential_references,
                   uint32_t influential_references_len,
                   corpusid_t* noninfluential_references,
                   uint32_t noninfluential_references_len)
{
    ssize_t s1 = sizeof(cpaper_fixed_data_t);
    ssize_t s2 = s1 + paper->fixed->titlelen;
    ssize_t s3 = s2 + paper->fixed->journallen;
    ssize_t s4 = s3 + paper->fixed->authorslen * sizeof(authorid_t);
    ssize_t s5 = s4 + abstractlen;
    ssize_t s6 = s5 + tldrlen;
    ssize_t s7 = s6 + influential_citations_len * sizeof(corpusid_t);
    ssize_t s8 = s7 + noninfluential_citations_len * sizeof(corpusid_t);
    ssize_t s9 = s8 + influential_references_len * sizeof(corpusid_t);
    ssize_t s10 = s9 + noninfluential_references_len * sizeof(corpusid_t);

    cpaper->fixed = (cpaper_fixed_data_t*)malloc(s10);
    if (!cpaper->fixed)
        abort();
    memcpy(&cpaper->fixed->sha1, sha1, sizeof(sha1_t));
    cpaper->fixed->titlelen = paper->fixed->titlelen;
    cpaper->fixed->journallen = paper->fixed->journallen;
    cpaper->fixed->authorslen = paper->fixed->authorslen;
    cpaper->fixed->abstractlen = abstractlen;
    cpaper->fixed->tldrlen = tldrlen;
    cpaper->fixed->influential_citations_len = influential_citations_len;
    cpaper->fixed->noninfluential_citations_len = noninfluential_citations_len;
    cpaper->fixed->influential_references_len = influential_references_len;
    cpaper->fixed->noninfluential_references_len =
      noninfluential_references_len;
    cpaper->fixed->pubyear = paper->fixed->pubyear;
    cpaper->fixed->pubmonth = paper->fixed->pubmonth;
    cpaper->fixed->pubday = paper->fixed->pubday;
    cpaper->fixed->isopenaccess = paper->fixed->isopenaccess;

    cpaper->title = (char*)cpaper->fixed + s1;
    cpaper->journal = (char*)cpaper->fixed + s2;
    cpaper->authors = (authorid_t*)((char*)cpaper->fixed + s3);
    cpaper->abstract = (char*)cpaper->fixed + s4;
    cpaper->tldr = (char*)cpaper->fixed + s5;
    cpaper->influential_citations = (corpusid_t*)((char*)cpaper->fixed + s6);
    cpaper->noninfluential_citations = (corpusid_t*)((char*)cpaper->fixed + s7);
    cpaper->influential_references = (corpusid_t*)((char*)cpaper->fixed + s8);
    cpaper->noninfluential_references =
      (corpusid_t*)((char*)cpaper->fixed + s9);

    memcpy(cpaper->title, paper->title, cpaper->fixed->titlelen);
    memcpy(cpaper->journal, paper->journal, cpaper->fixed->journallen);
    memcpy(cpaper->authors,
           paper->authors,
           cpaper->fixed->authorslen * sizeof(authorid_t));
    memcpy(cpaper->abstract, abstract, cpaper->fixed->abstractlen);
    memcpy(cpaper->tldr, tldr, cpaper->fixed->tldrlen);
    memcpy(cpaper->influential_citations,
           influential_citations,
           cpaper->fixed->influential_citations_len * sizeof(corpusid_t));
    memcpy(cpaper->noninfluential_citations,
           noninfluential_citations,
           cpaper->fixed->noninfluential_citations_len * sizeof(corpusid_t));
    memcpy(cpaper->influential_references,
           influential_references,
           cpaper->fixed->influential_references_len * sizeof(corpusid_t));
    memcpy(cpaper->noninfluential_references,
           noninfluential_references,
           cpaper->fixed->noninfluential_references_len * sizeof(corpusid_t));

    cpaper->all_citeref = cpaper->influential_citations; // first in memory
}

void
data_free_cpaper(cpaper_t* cpaper)
{
    free(cpaper->fixed);
}

void
data_cpaper_to_mdb_val(cpaper_t* cpaper, MDB_val* data)
{
    data->mv_size = sizeof(cpaper_fixed_data_t) + cpaper->fixed->titlelen +
                    cpaper->fixed->journallen +
                    cpaper->fixed->authorslen * sizeof(authorid_t) +
                    cpaper->fixed->abstractlen + cpaper->fixed->tldrlen +
                    (cpaper->fixed->influential_citations_len +
                     cpaper->fixed->noninfluential_citations_len +
                     cpaper->fixed->influential_references_len +
                     cpaper->fixed->noninfluential_references_len) *
                      sizeof(corpusid_t);
    data->mv_data = cpaper->fixed;
}

void
data_create_cauthor(cauthor_t* cauthor,
                    author_t* author,
                    corpusid_t* papers,
                    uint32_t paperslen)
{
    cauthor->fixed = (cauthor_fixed_data_t*)malloc(
      sizeof(cauthor_fixed_data_t) + author->authorlen +
      paperslen * sizeof(corpusid_t));
    cauthor->fixed->namelen = author->authorlen;
    cauthor->fixed->paperslen = paperslen;
    cauthor->name = ((char*)cauthor->fixed) + sizeof(cauthor_fixed_data_t);
    cauthor->papers =
      (corpusid_t*)(((char*)cauthor->fixed) + sizeof(cauthor_fixed_data_t) +
                    cauthor->fixed->namelen);

    memcpy(cauthor->name, author->author, cauthor->fixed->namelen);
    memcpy(cauthor->papers, papers, paperslen * sizeof(corpusid_t));
}

void
data_free_cauthor(cauthor_t* cauthor)
{
    free(cauthor->fixed);
}

void
data_cauthor_to_mdb_val(cauthor_t* cauthor, MDB_val* data)
{
    data->mv_size = sizeof(cauthor_fixed_data_t) + cauthor->fixed->namelen +
                    cauthor->fixed->paperslen * sizeof(corpusid_t);
    data->mv_data = cauthor->fixed;
}

void
data_mdb_val_to_cauthor(MDB_val* data, cauthor_t* cauthor)
{
    cauthor->fixed = (cauthor_fixed_data_t*)data->mv_data;
    cauthor->name = ((char*)cauthor->fixed) + sizeof(cauthor_fixed_data_t);
    cauthor->papers =
      (corpusid_t*)(((char*)cauthor->fixed) + sizeof(cauthor_fixed_data_t) +
                    cauthor->fixed->namelen);
}

sha1_t*
data_mdb_val_to_sha1(MDB_val* data)
{
    return (sha1_t*)data->mv_data;
}

void
data_sha1_to_mdb_val(sha1_t* sha1, MDB_val* data)
{
    data->mv_size = sizeof(sha1_t);
    data->mv_data = (void*)sha1;
}

void
data_abstract_to_mdb_val(abstract_t* abstract, MDB_val* data)
{
    data->mv_size = abstract->abstractlen;
    data->mv_data = abstract->abstract;
}

void
data_mdb_val_to_abstract(MDB_val* data, abstract_t* abstract)
{
    abstract->abstractlen = data->mv_size;
    abstract->abstract = data->mv_data;
}

void
data_tldr_to_mdb_val(tldr_t* tldr, MDB_val* data)
{
    data->mv_size = tldr->tldrlen;
    data->mv_data = tldr->tldr;
}

void
data_mdb_val_to_tldr(MDB_val* data, tldr_t* tldr)
{
    tldr->tldrlen = data->mv_size;
    tldr->tldr = data->mv_data;
}

void
data_author_to_mdb_val(author_t* author, MDB_val* data)
{
    data->mv_size = author->authorlen;
    data->mv_data = author->author;
}

void
data_mdb_val_to_author(MDB_val* data, author_t* author)
{
    author->authorlen = data->mv_size;
    author->author = data->mv_data;
}

#ifndef _HEADER_DATA
#define _HEADER_DATA 1

#include <assert.h>
#include <lmdb.h>
#include <stdint.h>

typedef uint32_t corpusid_t;
typedef uint32_t authorid_t;

struct paper_fixed_data_t
{
    uint32_t titlelen;
    uint32_t journallen;
    uint16_t authorslen;
    uint16_t pubyear;
    uint8_t pubmonth;
    uint8_t pubday;
    uint8_t isopenaccess;
};

typedef struct paper_fixed_data_t paper_fixed_data_t;

struct paper_t
{
    paper_fixed_data_t* fixed;
    char* title;
    char* journal;
    authorid_t* authors;
};

typedef struct paper_t paper_t;

struct sha1_t // useless struct only to have 160 bits size
{
    uint64_t first;
    uint64_t second;
    uint32_t third;
};

typedef struct sha1_t sha1_t;

struct cpaper_fixed_data_t
{
    sha1_t sha1;
    uint32_t titlelen;
    uint32_t journallen;
    uint16_t authorslen;
    uint32_t abstractlen;
    uint32_t tldrlen;
    uint32_t influential_citations_len;
    uint32_t noninfluential_citations_len;
    uint32_t influential_references_len;
    uint32_t noninfluential_references_len;
    uint16_t pubyear;
    uint8_t pubmonth;
    uint8_t pubday;
    uint8_t isopenaccess;
};

typedef struct cpaper_fixed_data_t cpaper_fixed_data_t;

struct cpaper_t
{
    cpaper_fixed_data_t* fixed;
    char* title;
    char* journal;
    authorid_t* authors;
    char* abstract;
    char* tldr;
    corpusid_t* all_citeref;
    corpusid_t* influential_citations;
    corpusid_t* noninfluential_citations;
    corpusid_t* influential_references;
    corpusid_t* noninfluential_references;
};

typedef struct cpaper_t cpaper_t;

struct abstract_t
{
    size_t abstractlen;
    char* abstract;
};

typedef struct abstract_t abstract_t;

struct tldr_t
{
    size_t tldrlen;
    char* tldr;
};

typedef struct tldr_t tldr_t;

struct author_t
{
    size_t authorlen;
    char* author;
};

typedef struct author_t author_t;

struct cauthor_fixed_data_t
{
    uint32_t namelen;
    uint32_t paperslen;
};

typedef struct cauthor_fixed_data_t cauthor_fixed_data_t;

struct cauthor_t
{
    cauthor_fixed_data_t* fixed;
    char* name;
    corpusid_t* papers;
};

typedef struct cauthor_t cauthor_t;

#endif

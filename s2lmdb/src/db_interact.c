#include "db_interact.h"

int
db_write_paper(db_t* db,
               corpusid_t corpusid,
               char* title,
               uint32_t titlelen,
               char* journal,
               uint32_t journallen,
               authorid_t* authors,
               uint16_t authorslen,
               uint16_t pubyear,
               uint8_t pubmonth,
               uint8_t pubday,
               uint8_t isopenaccess)
{
    int rc;
    rc = db_renew_all_dbi_if_needed(db);
    if (rc)
        return rc;
    MDB_val key, data;
    paper_t paper;
    data_corpusid_to_mdb_val(&corpusid, &key);
    data_create_paper(&paper,
                      title,
                      titlelen,
                      journal,
                      journallen,
                      authors,
                      authorslen,
                      pubyear,
                      pubmonth,
                      pubday,
                      isopenaccess);
    data_paper_to_mdb_val(&paper, &data);
    rc = mdb_put(db->txn, db->papers_dbi, &key, &data, 0);
    db->papers_counts++;
    data_free_paper(&paper);
    if (rc)
        return rc;

    data_corpusid_to_mdb_val(&corpusid, &data);
    ssize_t i;
    for (i = 0; i < authorslen; i++) {
        data_authorid_to_mdb_val(&authors[i], &key);
        rc = mdb_put(db->txn, db->authorships_by_authorid_dbi, &key, &data, 0);
        db->authorships_by_authorid_counts++;
        if (rc)
            return rc;
    }
    return (0);
}

int
db_write_paper_id(db_t* db, corpusid_t corpusid, sha1_t* sha1, int primary)
{
    int rc;
    rc = db_renew_all_dbi_if_needed(db);
    if (rc)
        return rc;
    MDB_val mdb_corpusid, mdb_sha1;
    data_corpusid_to_mdb_val(&corpusid, &mdb_corpusid);
    data_sha1_to_mdb_val(sha1, &mdb_sha1);
    rc =
      mdb_put(db->txn, db->paper_ids_by_sha1_dbi, &mdb_sha1, &mdb_corpusid, 0);
    db->paper_ids_by_sha1_counts++;
    if (rc)
        return rc;
    if (primary) {
        rc = mdb_put(
          db->txn, db->paper_ids_by_corpusid_dbi, &mdb_corpusid, &mdb_sha1, 0);
        db->paper_ids_by_corpusid_counts++;
    }
    return rc;
}

int
db_write_abstract(db_t* db,
                  corpusid_t corpusid,
                  char* zabstract,
                  ssize_t abstractlen)
{
    int rc;
    rc = db_renew_all_dbi_if_needed(db);
    if (rc)
        return rc;
    MDB_val key, val;
    data_corpusid_to_mdb_val(&corpusid, &key);
    abstract_t abstract;
    abstract.abstractlen = abstractlen;
    abstract.abstract = zabstract;
    data_abstract_to_mdb_val(&abstract, &val);

    rc = mdb_put(db->txn, db->abstracts_dbi, &key, &val, 0);
    db->abstracts_counts++;

    return rc;
}

int
db_write_tldr(db_t* db, corpusid_t corpusid, char* ztldr, ssize_t tldrlen)
{
    int rc;
    rc = db_renew_all_dbi_if_needed(db);
    if (rc)
        return rc;
    MDB_val key, val;
    data_corpusid_to_mdb_val(&corpusid, &key);
    tldr_t tldr;
    tldr.tldrlen = tldrlen;
    tldr.tldr = ztldr;
    data_tldr_to_mdb_val(&tldr, &val);

    rc = mdb_put(db->txn, db->tldrs_dbi, &key, &val, 0);
    db->tldrs_counts++;

    return rc;
}

int
db_write_author(db_t* db, authorid_t authorid, char* zauthor, ssize_t authorlen)
{
    int rc;
    rc = db_renew_all_dbi_if_needed(db);
    if (rc)
        return rc;
    MDB_val key, val;
    data_authorid_to_mdb_val(&authorid, &key);
    author_t author;
    author.authorlen = authorlen;
    author.author = zauthor;
    data_author_to_mdb_val(&author, &val);

    rc = mdb_put(db->txn, db->authors_dbi, &key, &val, 0);
    db->authors_counts++;

    return rc;
}

int
db_write_citation(db_t* db,
                  corpusid_t citingcorpusid,
                  corpusid_t citedcorpusid,
                  int influential)
{
    int rc;
    rc = db_renew_all_dbi_if_needed(db);
    if (rc)
        return rc;
    MDB_val mdb_citing, mdb_cited;
    data_authorid_to_mdb_val(&citingcorpusid, &mdb_citing);
    data_authorid_to_mdb_val(&citedcorpusid, &mdb_cited);

    if (influential) {
        rc = mdb_put(
          db->txn, db->influential_references_dbi, &mdb_citing, &mdb_cited, 0);
        db->influential_references_counts++;
        if (rc)
            return rc;
        rc = mdb_put(
          db->txn, db->influential_citations_dbi, &mdb_cited, &mdb_citing, 0);
        db->influential_citations_counts++;
        if (rc)
            return rc;
    } else {
        rc = mdb_put(db->txn,
                     db->noninfluential_references_dbi,
                     &mdb_citing,
                     &mdb_cited,
                     0);
        db->noninfluential_references_counts++;
        if (rc)
            return rc;
        rc = mdb_put(db->txn,
                     db->noninfluential_citations_dbi,
                     &mdb_cited,
                     &mdb_citing,
                     0);
        db->noninfluential_citations_counts++;
        if (rc)
            return rc;
    }
    return 0;
}

#ifndef _HEADER_DATA_MANIP
#define _HEADER_DATA_MANIP 1

#include <stdlib.h>
#include <string.h>

#include "data.h"

void
data_corpusid_to_mdb_val(corpusid_t* corpusid, MDB_val* val);

void
data_authorid_to_mdb_val(authorid_t* authorid, MDB_val* val);

void
data_mdb_val_to_paper(MDB_val* data, paper_t* paper);

void
data_create_paper(paper_t* paper,
                  char* title,
                  uint32_t titlelen,
                  char* journal,
                  uint32_t journallen,
                  authorid_t* authors,
                  uint16_t authorslen,
                  uint16_t pubyear,
                  uint8_t pubmonth,
                  uint8_t pubday,
                  uint8_t isopenaccess);

void
data_free_paper(paper_t* paper);

void
data_paper_to_mdb_val(paper_t* paper, MDB_val* data);

void
data_mdb_val_to_cpaper(MDB_val* data, cpaper_t* cpaper);

void
data_create_cpaper(cpaper_t* cpaper,
                   paper_t* paper,
                   sha1_t* sha1,
                   char* abstract,
                   uint32_t abstractlen,
                   char* tldr,
                   uint32_t tldrlen,
                   corpusid_t* influential_citations,
                   uint32_t influential_citations_len,
                   corpusid_t* noninfluential_citations,
                   uint32_t noninfluential_citations_len,
                   corpusid_t* influential_references,
                   uint32_t influential_references_len,
                   corpusid_t* noninfluential_references,
                   uint32_t noninfluential_references_len);

void
data_free_cpaper(cpaper_t* cpaper);

void
data_cpaper_to_mdb_val(cpaper_t* cpaper, MDB_val* data);

sha1_t*
data_mdb_val_to_sha1(MDB_val* data);

void
data_sha1_to_mdb_val(sha1_t* sha1, MDB_val* data);

void
data_abstract_to_mdb_val(abstract_t* abstract, MDB_val* data);

void
data_mdb_val_to_abstract(MDB_val* data, abstract_t* abstract);

void
data_tldr_to_mdb_val(tldr_t* tldr, MDB_val* data);

void
data_mdb_val_to_tldr(MDB_val* data, tldr_t* tldr);

void
data_author_to_mdb_val(author_t* author, MDB_val* data);

void
data_mdb_val_to_author(MDB_val* data, author_t* author);

void
data_create_cauthor(cauthor_t* cauthor,
                    author_t* author,
                    corpusid_t* papers,
                    uint32_t paperslen);

void
data_free_cauthor(cauthor_t* cauthor);

void
data_cauthor_to_mdb_val(cauthor_t* cauthor, MDB_val* data);

void
data_mdb_val_to_cauthor(MDB_val* data, cauthor_t* cauthor);

#endif

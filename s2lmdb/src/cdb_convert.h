#ifndef _HEADER_CDB_CONVERT
#define _HEADER_CDB_CONVERT 1

#include "cdb.h"
#include "db.h"

int
cdb_db_convert(cdb_t* cdb, db_t* db);

int
cdb_db_convert_papers(cdb_t* cdb, db_t* db);

int
cdb_db_convert_paper_ids(cdb_t* cdb, db_t* db);

int
cdb_db_convert_authors(cdb_t* cdb, db_t* db);

#endif

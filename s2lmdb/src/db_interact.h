#ifndef _HEADER_DB_INTERACT
#define _HEADER_DB_INTERACT 1

#include "db.h"

int
db_write_paper(db_t* db,
               corpusid_t corpusid,
               char* title,
               uint32_t titlelen,
               char* journal,
               uint32_t journallen,
               authorid_t* authors,
               uint16_t authorslen,
               uint16_t pubyear,
               uint8_t pubmonth,
               uint8_t pubday,
               uint8_t isopenaccess);

int
db_write_paper_id(db_t* db, corpusid_t corpusid, sha1_t* sha1, int primary);

int
db_write_abstract(db_t* db,
                  corpusid_t corpusid,
                  char* abstract,
                  ssize_t abstractlen);

int
db_write_tldr(db_t* db, corpusid_t corpusid, char* tldr, ssize_t tldrlen);

int
db_write_author(db_t* db, authorid_t authorid, char* author, ssize_t authorlen);

int
db_write_citation(db_t* db,
                  corpusid_t citingcorpusid,
                  corpusid_t citedcorpusid,
                  int influential);

#endif

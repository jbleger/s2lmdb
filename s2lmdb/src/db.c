#include "db.h"

#define MAX_INSERTS_INIT 0x7fffffffffffffff

int
db_set_existing_dbi_if_exists_else_new(db_t* db,
                                       char* name,
                                       MDB_dbi* dbi,
                                       uint64_t* counter,
                                       unsigned int flags)
{
    int rc;
    MDB_val key, data;
    key.mv_size = strlen(name) + 1;
    key.mv_data = name;
    MDB_cursor* cursor;
    rc = mdb_cursor_open(db->txn, db->assoc_dbi, &cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_get(cursor, &key, &data, MDB_SET);
    if (rc == MDB_NOTFOUND) {
        mdb_cursor_close(cursor);
        return db_new_dbi(db, name, dbi, counter, flags);
    } else if (rc) {
        mdb_cursor_close(cursor);
        return rc;
    }
    mdb_cursor_get(cursor, &key, &data, MDB_LAST_DUP);
    mdb_cursor_close(cursor);
    char* completename = data.mv_data;
    rc = mdb_dbi_open(db->txn, completename, flags, dbi);
    if (rc)
        return rc;

    MDB_stat stats;
    rc = mdb_stat(db->txn, *dbi, &stats);
    if (rc)
        return rc;
    *counter = stats.ms_entries;

    ssize_t i;
    for (i = data.mv_size - 2; i >= 0; i--)
        if (completename[i] == '_')
            break;
    uint64_t current_counter = atol(completename + i + 1);
    if (db->counter <= current_counter)
        db->counter = current_counter + 1;

    fprintf(stderr,
            "Reusing DBI, %s, %s, counter=%lu, global_counter=%lu\n",
            name,
            completename,
            *counter,
            db->counter);

    return 0;
}

int
db_renew_dbi(db_t* db,
             char* name,
             MDB_dbi* dbi,
             uint64_t* counter,
             unsigned int flags)
{
    int rc;
    if (*counter != MAX_INSERTS_INIT) {
        rc = db_commit_database(db);
        if (rc)
            return rc;
        mdb_dbi_close(db->env, *dbi);
        rc = db_new_dbi(db, name, dbi, counter, flags);
        return rc;
    } else {
        rc =
          db_set_existing_dbi_if_exists_else_new(db, name, dbi, counter, flags);
        return rc;
    }
}

int
db_new_dbi(db_t* db,
           char* name,
           MDB_dbi* dbi,
           uint64_t* counter,
           unsigned int flags)
{
    int rc;
    char strbuf[1024];
    unsigned int writed = snprintf(strbuf, 1024, "%s_%08lu", name, db->counter);
    MDB_val key, data;
    key.mv_size = strlen(name) + 1;
    key.mv_data = name;
    data.mv_size = writed + 1;
    data.mv_data = strbuf;
    rc = mdb_put(db->txn, db->assoc_dbi, &key, &data, 0);
    if (rc)
        return rc;
    db->counter++;
    rc = mdb_dbi_open(db->txn, strbuf, MDB_CREATE | flags, dbi);
    if (rc)
        return rc;
    *counter = 0;
    return 0;
}

inline uint64_t
db_max_ceil(uint64_t val)
{
    return ((val < MAX_ENTRIES) ? val : MAX_ENTRIES);
}

int
db_renew_all_dbi_if_needed(db_t* db)
{
    int rc;

    if (db->papers_counts >= db_max_ceil(db->ramGB * MAX_ENTRIES_PAPERS_CST)) {
        rc = db_renew_dbi(
          db, "papers", &db->papers_dbi, &db->papers_counts, MDB_INTEGERKEY);
        if (rc)
            return (rc);
    }

    if (db->paper_ids_by_corpusid_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_IDS_CST)) {
        rc = db_renew_dbi(db,
                          "paper_ids_by_corpusid",
                          &db->paper_ids_by_corpusid_dbi,
                          &db->paper_ids_by_corpusid_counts,
                          MDB_INTEGERKEY);
        if (rc)
            return (rc);
    }

    if (db->paper_ids_by_sha1_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_IDS_CST)) {
        rc = db_renew_dbi(db,
                          "paper_ids_by_sha1",
                          &db->paper_ids_by_sha1_dbi,
                          &db->paper_ids_by_sha1_counts,
                          0);
        if (rc)
            return (rc);
    }

    if (db->abstracts_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_ABSTRACTS_CST)) {
        rc = db_renew_dbi(db,
                          "paper_abstracts",
                          &db->abstracts_dbi,
                          &db->abstracts_counts,
                          MDB_INTEGERKEY);
        if (rc)
            return (rc);
    }

    if (db->tldrs_counts >= db_max_ceil(db->ramGB * MAX_ENTRIES_TLDRS_CST)) {
        rc = db_renew_dbi(
          db, "paper_tldrs", &db->tldrs_dbi, &db->tldrs_counts, MDB_INTEGERKEY);
        if (rc)
            return (rc);
    }

    if (db->authorships_by_authorid_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_AUTHORSCITREFS_CST)) {
        rc = db_renew_dbi(db,
                          "authorships_by_authorid",
                          &db->authorships_by_authorid_dbi,
                          &db->authorships_by_authorid_counts,
                          MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
        if (rc)
            return (rc);
    }

    if (db->authors_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_AUTHORS_CST)) {
        rc = db_renew_dbi(
          db, "authors", &db->authors_dbi, &db->authors_counts, MDB_INTEGERKEY);
        if (rc)
            return (rc);
    }

    if (db->influential_citations_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_AUTHORSCITREFS_CST)) {
        rc = db_renew_dbi(db,
                          "influential_citations",
                          &db->influential_citations_dbi,
                          &db->influential_citations_counts,
                          MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
        if (rc)
            return (rc);
    }

    if (db->noninfluential_citations_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_AUTHORSCITREFS_CST)) {
        rc = db_renew_dbi(db,
                          "noninfluential_citations",
                          &db->noninfluential_citations_dbi,
                          &db->noninfluential_citations_counts,
                          MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
        if (rc)
            return (rc);
    }

    if (db->influential_references_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_AUTHORSCITREFS_CST)) {
        rc = db_renew_dbi(db,
                          "influential_references",
                          &db->influential_references_dbi,
                          &db->influential_references_counts,
                          MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
        if (rc)
            return (rc);
    }

    if (db->noninfluential_references_counts >=
        db_max_ceil(db->ramGB * MAX_ENTRIES_AUTHORSCITREFS_CST)) {
        rc = db_renew_dbi(db,
                          "noninfluential_references",
                          &db->noninfluential_references_dbi,
                          &db->noninfluential_references_counts,
                          MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
        if (rc)
            return (rc);
    }

    return 0;
}

int
db_open_database(db_t* db, char* pathname, uint64_t ramGB, int rw)
{
    int rc;

    rc = mdb_env_create(&db->env);
    if (rc)
        return (rc);

    rc = mdb_env_set_mapsize(db->env, MAXIMUM_LMDB_SIZE);
    if (rc)
        return (rc);

    rc = mdb_env_set_maxdbs(db->env, 16777216);
    if (rc)
        return (rc);

    // MDB_NOSUBDIR | MDB_NORDAHEAD | MDB_NOMETASYNC | MDB_NOSYNC | MDB_NOLOCK,
    rc = mdb_env_open(db->env,
                      pathname,
                      MDB_NOSUBDIR | MDB_NOMETASYNC | MDB_NOSYNC | MDB_NOLOCK | MDB_NORDAHEAD,
                      0644);
    if (rc)
        return (rc);

    if (rw)
        rc = mdb_txn_begin(db->env, NULL, 0, &db->txn);
    else
        rc = mdb_txn_begin(db->env, NULL, MDB_RDONLY, &db->txn);
    if (rc)
        return (rc);

    rc = mdb_dbi_open(
      db->txn, "aaassoc", MDB_CREATE | MDB_DUPSORT, &db->assoc_dbi);
    if (rc)
        return (rc);

    db->ramGB = ramGB;

    db->counter = 0;
    db->papers_counts = MAX_INSERTS_INIT;
    db->paper_ids_by_corpusid_counts = MAX_INSERTS_INIT;
    db->paper_ids_by_sha1_counts = MAX_INSERTS_INIT;
    db->abstracts_counts = MAX_INSERTS_INIT;
    db->tldrs_counts = MAX_INSERTS_INIT;
    db->authorships_by_authorid_counts = MAX_INSERTS_INIT;
    db->authors_counts = MAX_INSERTS_INIT;
    db->influential_citations_counts = MAX_INSERTS_INIT;
    db->noninfluential_citations_counts = MAX_INSERTS_INIT;
    db->influential_references_counts = MAX_INSERTS_INIT;
    db->noninfluential_references_counts = MAX_INSERTS_INIT;

    rc = db_renew_all_dbi_if_needed(db);
    if (rc)
        return (rc);
    return 0;
}

int
db_commit_database(db_t* db)
{

    int rc;
    rc = mdb_txn_commit(db->txn);
    if (rc)
        return rc;
    rc = mdb_txn_begin(db->env, NULL, 0, &db->txn);
    return rc;
}

void
db_close_database(db_t* db)
{
    mdb_txn_abort(db->txn);
    mdb_close(db->env, db->papers_dbi);
    mdb_env_close(db->env);
}

#include "db_merge.h"

inline int compare_lt_key_and_val(db_t* db, MDB_dbi dbi, MDB_val* keyA, MDB_val* keyB, MDB_val* dataA, MDB_val* dataB, unsigned int dup)
{
    if(!keyA->mv_data)
        return 0;
    if(!keyB->mv_data)
        return 1;
    int cmp_keys = mdb_cmp(db->txn, dbi, keyA, keyB);
    if(cmp_keys<0)
        return 1;
    if(cmp_keys>0)
        return 0;
    if(!dup)
        return 0;
    int cmp_datas = mdb_dcmp(db->txn, dbi, dataA, dataB);
    if(cmp_datas<0)
        return 1;
    return 0;
}


/* Taken from CPython: https://github.com/python/cpython/blob/3.11/Lib/heapq.py
 *
 *  def _siftup(heap, pos):
        endpos = len(heap)
        startpos = pos
        newitem = heap[pos]
        # Bubble up the smaller child until hitting a leaf.
        childpos = 2*pos + 1    # leftmost child position
        while childpos < endpos:
            # Set childpos to index of smaller child.
            rightpos = childpos + 1
            if rightpos < endpos and not heap[childpos] < heap[rightpos]:
                childpos = rightpos
            # Move the smaller child up.
            heap[pos] = heap[childpos]
            pos = childpos
            childpos = 2*pos + 1
        # The leaf at pos is empty now.  Put newitem there, and bubble it up
        # to its final resting place (by sifting its parents down).
        heap[pos] = newitem
        _siftdown(heap, startpos, pos)

    def _siftdown(heap, startpos, pos):
        newitem = heap[pos]
        # Follow the path to the root, moving parents down until finding a place
        # newitem fits.
        while pos > startpos:
            parentpos = (pos - 1) >> 1
            parent = heap[parentpos]
            if newitem < parent:
                heap[pos] = parent
                pos = parentpos
                continue
            break
        heap[pos] = newitem
 */



void heap_siftupdown(db_t* db,
        MDB_dbi dbi,
        size_t pos,
        size_t* idxs,
        size_t endpos,
        MDB_val* keys,
        MDB_val* datas,
        unsigned int dup)
{
    size_t startpos = pos;
    size_t newitem = idxs[pos];
    size_t childpos = 2*pos+1;
    size_t rightpos;
    int ischildlessthanright;
    while(childpos<endpos)
    {
        rightpos = childpos+1;
        if(rightpos<endpos)
        {
            ischildlessthanright = compare_lt_key_and_val(db, dbi, &keys[idxs[childpos]], &keys[idxs[rightpos]], &datas[idxs[childpos]], &datas[idxs[rightpos]], dup);
            if(!ischildlessthanright)
            childpos = rightpos;
        }
        idxs[pos] = idxs[childpos];
        pos = childpos;
        childpos = 2*pos+1;
    }
    idxs[pos] = newitem;

    newitem = idxs[pos];
    size_t parentpos;
    size_t parent;
    int newitemlessthanparent;
    while(pos>startpos)
    {
        parentpos = (pos-1)>>1;
        parent = idxs[parentpos];
        newitemlessthanparent = compare_lt_key_and_val(db, dbi, &keys[newitem], &keys[parent], &datas[newitem], &datas[parent], dup);
        if(newitemlessthanparent)
        {
            idxs[pos] = parent;
            pos=parentpos;
            continue;
        }
        break;
    }
    idxs[pos] = newitem;
}



int
db_merge_dbis(db_t* db,
              MDB_dbi* dbis,
              size_t ndbis,
              MDB_dbi dbi,
              unsigned int flags,
              unsigned long int total_iter)
{
    int rc;
    unsigned int dup = flags & MDB_DUPSORT;
    unsigned int append_flag = (flags?MDB_APPENDDUP:MDB_APPEND);
    MDB_cursor** cursors;
    MDB_val* keys;
    MDB_val* datas;
    size_t* heap;
    cursors = malloc(ndbis * sizeof(MDB_cursor*));
    if (!cursors)
        abort();
    keys = malloc(ndbis * sizeof(MDB_val));
    if (!keys)
        abort();
    datas = malloc(ndbis * sizeof(MDB_val));
    if (!datas)
        abort();
    heap = malloc(ndbis * sizeof(size_t));
    if (!heap)
        abort();

    size_t i;

    for (i = 0; i < ndbis; i++) {
        rc = mdb_cursor_open(db->txn, dbis[i], &cursors[i]);
        if (rc)
            goto finalize_free;
    }

    for (i = 0; i < ndbis; i++) {
        heap[i]=i;
        rc = mdb_cursor_get(cursors[i], &keys[i], &datas[i], MDB_FIRST);
        if (rc == MDB_NOTFOUND)
            keys[i].mv_data = NULL;
        else if (rc)
            goto finalize_cursors;
    }

    size_t j;
    for(i=0;i<ndbis/2;i++)
    {
        j = ndbis/2-1-i;
        heap_siftupdown(db, dbi, j, heap, ndbis, keys, datas, dup);
        if(2*j+1<ndbis && compare_lt_key_and_val(db, dbi, &keys[heap[2*j+1]], &keys[heap[j]], &datas[heap[2*j+1]], &datas[heap[j]], dup))
        {
            rc = 12345;
            goto finalize_cursors;
        }
        if(2*j+2<ndbis && compare_lt_key_and_val(db, dbi, &keys[heap[2*j+2]], &keys[heap[j]], &datas[heap[2*j+2]], &datas[heap[j]], dup))
        {
            rc = 12345;
            goto finalize_cursors;
        }
    }

    size_t icurrentmin;
    MDB_val last_key, last_data;
    last_key.mv_data = NULL;
    unsigned long int iter = 0;
    unsigned long int dropped = 0;
    while (1) {
        if(!keys[heap[0]].mv_data)
            break;

        icurrentmin = heap[0];

        iter++;
        if((!last_key.mv_data)||compare_lt_key_and_val(db, dbi, &last_key, &keys[icurrentmin], &last_data, &datas[icurrentmin], dup))
        {
            rc = mdb_put(db->txn, dbi, &keys[icurrentmin], &datas[icurrentmin], append_flag);
            if (rc)
                goto finalize_cursors;
            last_key.mv_size = keys[icurrentmin].mv_size;
            last_key.mv_data = keys[icurrentmin].mv_data;
            last_data.mv_size = datas[icurrentmin].mv_size;
            last_data.mv_data = datas[icurrentmin].mv_data;
        }
        else
            dropped++;

        rc = mdb_cursor_get(cursors[icurrentmin],
                            &keys[icurrentmin],
                            &datas[icurrentmin],
                            MDB_NEXT);
        if (rc == MDB_NOTFOUND)
            keys[icurrentmin].mv_data = NULL;
        else if (rc)
            goto finalize_cursors;

        heap_siftupdown(db, dbi, 0, heap, ndbis, keys, datas, dup);

        if((iter%10000)==0)
            fprintf(stderr, "\r%9ld.%02ld/%ld.%02ld Mit, %10ld dropped", iter/1000000, (iter/10000)%100, total_iter/1000000, (total_iter/10000)%100, dropped);
    }

    fprintf(stderr,"\rit = %ld,    inserted = %ld,    dropped=%ld               \n", iter, iter-dropped, dropped);
    rc=0;

finalize_cursors:
    for (i = 0; i < ndbis; i++)
        mdb_cursor_close(cursors[i]);

finalize_free:
    free(cursors);
    free(keys);
    free(datas);
    free(heap);
    return rc;
}

int
db_compact_dbi(
        db_t* db,
        MDB_dbi dbi_from,
        MDB_dbi dbi_to,
        unsigned int flags)
{
    int rc;
    unsigned int append_flag = ((flags&MDB_DUPSORT)?MDB_APPENDDUP:MDB_APPEND);
    MDB_cursor* cursor_from;
    rc = mdb_cursor_open(db->txn, dbi_from, &cursor_from);
    if(rc) return rc;

    MDB_val key, data;
    rc = mdb_cursor_get(cursor_from, &key, &data, MDB_FIRST);
    if(rc==MDB_NOTFOUND)
    {
        rc=0;
        goto finalize_cursor;
    }
    else if(rc)
        goto finalize_cursor;

    while(1)
    {
        rc = mdb_put(db->txn, dbi_to, &key, &data, append_flag);
        if(rc)
            goto finalize_cursor;

        rc = mdb_cursor_get(cursor_from, &key, &data, MDB_NEXT);
        if(rc==MDB_NOTFOUND)
        {
            rc=0;
            break;
        }
        else if(rc)
            goto finalize_cursor;
    }

finalize_cursor:
    mdb_cursor_close(cursor_from);
    return rc;
}


int
db_count_assocs(
        db_t* db,
        char* name,
        size_t* ntab)
{
    int rc;
    MDB_val key, data;

    MDB_cursor* assoc_cursor;
    rc = mdb_cursor_open(db->txn, db->assoc_dbi, &assoc_cursor);
    if (rc)
        return rc;

    rc = mdb_cursor_get(assoc_cursor, &key, &data, MDB_FIRST);
    if (rc) {
        mdb_cursor_close(assoc_cursor);
        return rc;
    }

    key.mv_size = strlen(name) + 1;
    key.mv_data = name;

    rc = mdb_cursor_get(assoc_cursor, &key, &data, MDB_SET_KEY);
    if (rc) {
        mdb_cursor_close(assoc_cursor);
        return rc;
    }

    mdb_cursor_count(assoc_cursor, ntab);
    mdb_cursor_close(assoc_cursor);
    return 0;
}


int
db_do_compact(db_t* db,
             char* name,
             MDB_dbi* dbi,
             uint64_t* counter,
             unsigned int flags,
             int dryrun)
{
    int rc;
    MDB_val key, data;

    size_t ntab;
    db_count_assocs(db, name, &ntab);

    if (ntab <= 1)
        return 0;

    if (dryrun)
        return 1;

    size_t i;

    MDB_dbi current_dbi;
    for (i = 0; i < ntab; i++) {
        fprintf(stderr, "\rCompact dbs %s: %lu/%lu", name, i, ntab);
        key.mv_size = strlen(name) + 1;
        key.mv_data = name;
        rc = mdb_get(db->txn, db->assoc_dbi, &key, &data);
        if (rc)
            goto finalize;

        rc = mdb_dbi_open(db->txn, (char*)data.mv_data, flags, &current_dbi);
        if (rc)
            goto finalize;

        rc = db_new_dbi(db, name, dbi, counter, flags);
        if (rc)
            goto finalize;

        rc = db_compact_dbi(db, current_dbi, *dbi, flags);
        if (rc)
            goto finalize;

        mdb_del(db->txn, db->assoc_dbi, &key, &data);

        rc = mdb_drop(db->txn, current_dbi, 1);
        if (rc)
            goto finalize;

        rc = db_commit_database(db);
        if (rc)
            goto finalize;
    }
    fprintf(stderr, "\rCompacted %lu dbs %s         \n", ntab, name);

finalize:
    if (rc)
        return rc;
    return DB_DID_MERGE;
}

int
db_do_merge(db_t* db,
             char* name,
             MDB_dbi* dbi,
             uint64_t* counter,
             unsigned int flags,
             int dryrun)
{
    int rc;
    MDB_val key, data;

    size_t ntab;
    db_count_assocs(db, name, &ntab);

    if (ntab <= 1)
        return 0;

    if (dryrun)
        return 1;

    size_t i;
    unsigned long int total=0;
    MDB_stat stats;
    MDB_dbi* dbis;
    dbis = malloc(ntab * sizeof(MDB_dbi*));
    if (!dbi)
        abort();

    fprintf(stderr, "Merge %lu dbs %s to ..._%lu:\n", ntab, name, db->counter);
    for (i = 0; i < ntab; i++) {
        key.mv_size = strlen(name) + 1;
        key.mv_data = name;
        rc = mdb_get(db->txn, db->assoc_dbi, &key, &data);
        if (rc)
            goto finalize;

        rc = mdb_dbi_open(db->txn, (char*)data.mv_data, flags, &dbis[i]);
        if (rc)
            goto finalize;

        mdb_stat(db->txn, dbis[i], &stats);
        total += stats.ms_entries;
        mdb_del(db->txn, db->assoc_dbi, &key, &data);
    }

    rc = db_new_dbi(db, name, dbi, counter, flags);
    if (rc)
        goto finalize;

    rc = db_merge_dbis(db, dbis, ntab, *dbi, flags, total);
    if (rc)
        goto finalize;

    for (i = 0; i < ntab; i++) {
        fprintf(stderr, "\rDrop compacted dbs: %lu/%lu", i, ntab);
        rc = mdb_drop(db->txn, dbis[i], 1);
        if (rc)
            goto finalize;
    }
    fprintf(stderr, "\rDrop compacted dbs: %lu/%lu\n", ntab, ntab);

finalize:
    free(dbis);
    if (rc)
        return rc;
    return DB_DID_MERGE;
}

int
db_compactmerge_one(db_t* db,
                       char* name,
                       MDB_dbi* dbi,
                       uint64_t* counter,
                       unsigned int flags)
{
    int rc;
    rc = db_do_compact(db, name, dbi, counter, flags, 0);
    if (rc != DB_DID_MERGE) {
            if (rc)
                return rc;
    }
    rc = db_commit_database(db);
    if (rc)
        return rc;
    rc = db_do_merge(db, name, dbi, counter, flags, 0);
    if (rc != DB_DID_MERGE) {
            if (rc)
                return rc;
    }
    rc = db_commit_database(db);
    if (rc)
        return rc;

    return 0;
}

int
db_merge_all_until_one(db_t* db)
{
    int rc;


    rc = db_compactmerge_one(
      db, "papers", &db->papers_dbi, &db->papers_counts, MDB_INTEGERKEY);
    if (rc)
        return (rc);


    rc = db_compactmerge_one(db,
                                "paper_ids_by_corpusid",
                                &db->paper_ids_by_corpusid_dbi,
                                &db->paper_ids_by_corpusid_counts,
                                MDB_INTEGERKEY);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(db,
                                "paper_ids_by_sha1",
                                &db->paper_ids_by_sha1_dbi,
                                &db->paper_ids_by_sha1_counts,
                                0);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(db,
                                "paper_abstracts",
                                &db->abstracts_dbi,
                                &db->abstracts_counts,
                                MDB_INTEGERKEY);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(
      db, "paper_tldrs", &db->tldrs_dbi, &db->tldrs_counts, MDB_INTEGERKEY);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(db,
                                "authorships_by_authorid",
                                &db->authorships_by_authorid_dbi,
                                &db->authorships_by_authorid_counts,
                                MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(
      db, "authors", &db->authors_dbi, &db->authors_counts, MDB_INTEGERKEY);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(db,
                                "influential_citations",
                                &db->influential_citations_dbi,
                                &db->influential_citations_counts,
                                MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(db,
                                "noninfluential_citations",
                                &db->noninfluential_citations_dbi,
                                &db->noninfluential_citations_counts,
                                MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(db,
                                "influential_references",
                                &db->influential_references_dbi,
                                &db->influential_references_counts,
                                MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
    if (rc)
        return (rc);

    rc = db_compactmerge_one(db,
                                "noninfluential_references",
                                &db->noninfluential_references_dbi,
                                &db->noninfluential_references_counts,
                                MDB_INTEGERKEY | MDB_DUPSORT | MDB_INTEGERDUP);
    if (rc)
        return (rc);

    return 0;
}

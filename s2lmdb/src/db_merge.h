#ifndef _HEADER_DB_MERGE
#define _HEADER_DB_MERGE 1

#include "db.h"

#define DB_DID_MERGE 1431121

int
db_merge_all_until_one(db_t* db);

#endif

#include "cdb_convert.h"

int
cdb_db_convert(cdb_t* cdb, db_t* db)
{
    int rc;

    fprintf(stderr, "Conversion of papers\n");
    rc = cdb_db_convert_papers(cdb, db);
    if (rc)
        return rc;

    fprintf(stderr, "Conversion of papers ids\n");
    rc = cdb_db_convert_paper_ids(cdb, db);
    if (rc)
        return rc;

    fprintf(stderr, "Conversion of authors\n");
    rc = cdb_db_convert_authors(cdb, db);
    if (rc)
        return rc;

    return 0;
}

int
cdb_db_convert_paper_ids(cdb_t* cdb, db_t* db)
{
    int rc;
    MDB_cursor* paper_ids_cursor;
    MDB_val key, data;

    rc = mdb_cursor_open(db->txn, db->paper_ids_by_sha1_dbi, &paper_ids_cursor);
    if (rc)
        return rc;

    rc = mdb_cursor_get(paper_ids_cursor, &key, &data, MDB_FIRST);
    if (rc)
        goto cpaper_ids_finalize;

    unsigned long int iter = 0;
    while (1) {
        if((iter%10000)==0)
            fprintf(stderr, "\r  %ld.%02ld Mit", iter/1000000, (iter/10000)%100);

        rc = mdb_put(cdb->txn, cdb->paper_ids_dbi, &key, &data, MDB_APPEND);
        if (rc)
            goto cpaper_ids_finalize;

        iter++;

        rc = mdb_cursor_get(paper_ids_cursor, &key, &data, MDB_NEXT);
        if (rc) {
            if (rc == MDB_NOTFOUND)
                rc = 0;
            goto cpaper_ids_finalize;
        }
    }
    fprintf(stderr, "\r inserted  %ld it\n", iter);

cpaper_ids_finalize:
    mdb_cursor_close(paper_ids_cursor);
    return rc;
}

int
cdb_db_convert_authors(cdb_t* cdb, db_t* db)
{
    int rc;

    MDB_cursor *authors_cursor, *authorships_cursor;
    MDB_val authors_key, authors_data, authorships_key, authorships_data;
    MDB_val cauthor_data;

    rc = mdb_cursor_open(db->txn, db->authors_dbi, &authors_cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_open(
      db->txn, db->authorships_by_authorid_dbi, &authorships_cursor);
    if (rc)
        return rc;

    rc = mdb_cursor_get(authors_cursor, &authors_key, &authors_data, MDB_FIRST);
    if (rc)
        goto cpaper_authors_finalize;

    rc = mdb_cursor_get(
      authorships_cursor, &authorships_key, &authorships_data, MDB_FIRST);
    if (rc)
        goto cpaper_authors_finalize;

    size_t i;
    size_t paperslen;
    corpusid_t* papers;
    author_t author;
    cauthor_t cauthor;
    unsigned long int iter = 0;
    while (1) {
        if((iter%10000)==0)
            fprintf(stderr, "\r  %ld.%02ld Mit", iter/1000000, (iter/10000)%100);
        iter++;
        paperslen = 0;
        while (
          mdb_cmp(db->txn, db->authors_dbi, &authorships_key, &authors_key) <
          0) {
            rc = mdb_cursor_get(authorships_cursor,
                                &authorships_key,
                                &authorships_data,
                                MDB_NEXT_NODUP);
            if (rc)
                break;
        }
        if (rc != 0 && rc != MDB_NOTFOUND)
            goto cpaper_authors_finalize;
        if (rc != MDB_NOTFOUND &&
            mdb_cmp(db->txn, db->authors_dbi, &authorships_key, &authors_key) ==
              0) {
            rc = mdb_cursor_count(authorships_cursor, &paperslen);
            if (rc)
                goto cpaper_authors_finalize;
        }

        papers = (corpusid_t*)malloc(paperslen * sizeof(corpusid_t));

        for (i = 0; i < paperslen;) {
            papers[i] = *(corpusid_t*)authorships_data.mv_data;
            i++;
            if (i < paperslen) {
                rc = mdb_cursor_get(authorships_cursor,
                                    &authorships_key,
                                    &authorships_data,
                                    MDB_NEXT_DUP);
                if (rc) {
                    free(papers);
                    goto cpaper_authors_finalize;
                }
            }
        }

        data_mdb_val_to_author(&authors_data, &author);
        data_create_cauthor(&cauthor, &author, papers, paperslen);
        free(papers);

        data_cauthor_to_mdb_val(&cauthor, &cauthor_data);
        rc = mdb_put(
          cdb->txn, cdb->authors_dbi, &authors_key, &cauthor_data, MDB_APPEND);
        data_free_cauthor(&cauthor);

        if (rc)
            goto cpaper_authors_finalize;

        rc =
          mdb_cursor_get(authors_cursor, &authors_key, &authors_data, MDB_NEXT);
        if (rc == MDB_NOTFOUND) {
            rc = 0;
            break;
        }
        if (rc)
            goto cpaper_authors_finalize;
    }
    fprintf(stderr, "\r inserted  %ld it\n", iter);

cpaper_authors_finalize:
    mdb_cursor_close(authors_cursor);
    mdb_cursor_close(authorships_cursor);
    return rc;
}

int
cdb_db_convert_papers(cdb_t* cdb, db_t* db)
{
    int rc;
    MDB_cursor *papers_cursor, *paper_ids_cursor, *abstracts_cursor,
      *tldrs_cursor, *influential_citations_cursor,
      *noninfluential_citations_cursor, *influential_references_cursor,
      *noninfluential_references_cursor;

    rc = mdb_cursor_open(db->txn, db->papers_dbi, &papers_cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_open(
      db->txn, db->paper_ids_by_corpusid_dbi, &paper_ids_cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_open(db->txn, db->abstracts_dbi, &abstracts_cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_open(db->txn, db->tldrs_dbi, &tldrs_cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_open(
      db->txn, db->influential_citations_dbi, &influential_citations_cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_open(db->txn,
                         db->noninfluential_citations_dbi,
                         &noninfluential_citations_cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_open(
      db->txn, db->influential_references_dbi, &influential_references_cursor);
    if (rc)
        return rc;
    rc = mdb_cursor_open(db->txn,
                         db->noninfluential_references_dbi,
                         &noninfluential_references_cursor);
    if (rc)
        return rc;

    MDB_val key_papers, key_paper_ids, key_abstracts, key_tldrs,
      key_influential_citations, key_noninfluential_citations,
      key_influential_references, key_noninfluential_references;
    MDB_val data_papers, data_paper_ids, data_abstracts, data_tldrs,
      data_influential_citations, data_noninfluential_citations,
      data_influential_references, data_noninfluential_references;

    rc = mdb_cursor_get(papers_cursor, &key_papers, &data_papers, MDB_FIRST);
    if (rc)
        goto cpaper_finalize;
    rc = mdb_cursor_get(
      paper_ids_cursor, &key_paper_ids, &data_paper_ids, MDB_FIRST);
    if (rc)
        goto cpaper_finalize;
    rc = mdb_cursor_get(
      abstracts_cursor, &key_abstracts, &data_abstracts, MDB_FIRST);
    if (rc)
        goto cpaper_finalize;
    rc = mdb_cursor_get(tldrs_cursor, &key_tldrs, &data_tldrs, MDB_FIRST);
    if (rc)
        goto cpaper_finalize;
    rc = mdb_cursor_get(influential_citations_cursor,
                        &key_influential_citations,
                        &data_influential_citations,
                        MDB_FIRST);
    if (rc)
        goto cpaper_finalize;
    rc = mdb_cursor_get(noninfluential_citations_cursor,
                        &key_noninfluential_citations,
                        &data_noninfluential_citations,
                        MDB_FIRST);
    if (rc)
        goto cpaper_finalize;
    rc = mdb_cursor_get(influential_references_cursor,
                        &key_influential_references,
                        &data_influential_references,
                        MDB_FIRST);
    if (rc)
        goto cpaper_finalize;
    rc = mdb_cursor_get(noninfluential_references_cursor,
                        &key_noninfluential_references,
                        &data_noninfluential_references,
                        MDB_FIRST);
    if (rc)
        goto cpaper_finalize;

    corpusid_t *all_citeref, *influential_citations, *noninfluential_citations,
      *influential_references, *noninfluential_references;
    size_t i;
    paper_t paper;
    abstract_t abstract;
    tldr_t tldr;
    cpaper_t cpaper;
    MDB_val data_cpapers;
    sha1_t invalid_sha1;
    memset(&invalid_sha1, 0, sizeof(sha1_t));

    int has_id, has_abstract, has_tldr;
    size_t influential_citations_len, noninfluential_citations_len,
      influential_references_len, noninfluential_references_len;
    unsigned long int iter = 0;
    while (1) {
        if((iter%10000)==0)
            fprintf(stderr, "\r  %ld.%02ld Mit", iter/1000000, (iter/10000)%100);
        iter++;
        has_id = has_abstract = has_tldr = 0;
        influential_citations_len = noninfluential_citations_len =
          influential_references_len = noninfluential_references_len = 0;

        while (mdb_cmp(db->txn, db->papers_dbi, &key_paper_ids, &key_papers) <
               0) {
            rc = mdb_cursor_get(
              paper_ids_cursor, &key_paper_ids, &data_paper_ids, MDB_NEXT);
            if (rc)
                break;
        }
        if (rc != 0 && rc != MDB_NOTFOUND)
            goto cpaper_finalize;
        if (rc != MDB_NOTFOUND &&
            mdb_cmp(db->txn, db->papers_dbi, &key_paper_ids, &key_papers) == 0)
            has_id = 1;

        while (mdb_cmp(db->txn, db->papers_dbi, &key_abstracts, &key_papers) <
               0) {
            rc = mdb_cursor_get(
              abstracts_cursor, &key_abstracts, &data_abstracts, MDB_NEXT);
            if (rc)
                break;
        }
        if (rc != 0 && rc != MDB_NOTFOUND)
            goto cpaper_finalize;
        if (rc != MDB_NOTFOUND &&
            mdb_cmp(db->txn, db->papers_dbi, &key_abstracts, &key_papers) == 0)
            has_abstract = 1;

        while (mdb_cmp(db->txn, db->papers_dbi, &key_tldrs, &key_papers) < 0) {
            rc =
              mdb_cursor_get(tldrs_cursor, &key_tldrs, &data_tldrs, MDB_NEXT);
            if (rc)
                break;
        }
        if (rc != 0 && rc != MDB_NOTFOUND)
            goto cpaper_finalize;
        if (rc != MDB_NOTFOUND &&
            mdb_cmp(db->txn, db->papers_dbi, &key_tldrs, &key_papers) == 0)
            has_tldr = 1;

        while (mdb_cmp(db->txn,
                       db->papers_dbi,
                       &key_influential_citations,
                       &key_papers) < 0) {
            rc = mdb_cursor_get(influential_citations_cursor,
                                &key_influential_citations,
                                &data_influential_citations,
                                MDB_NEXT_NODUP);
            if (rc)
                break;
        }
        if (rc != 0 && rc != MDB_NOTFOUND)
            goto cpaper_finalize;
        if (rc != MDB_NOTFOUND && mdb_cmp(db->txn,
                                          db->papers_dbi,
                                          &key_influential_citations,
                                          &key_papers) == 0) {
            rc = mdb_cursor_count(influential_citations_cursor,
                                  &influential_citations_len);
            if (rc)
                goto cpaper_finalize;
        }

        while (mdb_cmp(db->txn,
                       db->papers_dbi,
                       &key_noninfluential_citations,
                       &key_papers) < 0) {
            rc = mdb_cursor_get(noninfluential_citations_cursor,
                                &key_noninfluential_citations,
                                &data_noninfluential_citations,
                                MDB_NEXT_NODUP);
            if (rc)
                break;
        }
        if (rc != 0 && rc != MDB_NOTFOUND)
            goto cpaper_finalize;
        if (rc != MDB_NOTFOUND && mdb_cmp(db->txn,
                                          db->papers_dbi,
                                          &key_noninfluential_citations,
                                          &key_papers) == 0) {
            rc = mdb_cursor_count(noninfluential_citations_cursor,
                                  &noninfluential_citations_len);
            if (rc)
                goto cpaper_finalize;
        }

        while (mdb_cmp(db->txn,
                       db->papers_dbi,
                       &key_influential_references,
                       &key_papers) < 0) {
            rc = mdb_cursor_get(influential_references_cursor,
                                &key_influential_references,
                                &data_influential_references,
                                MDB_NEXT_NODUP);
            if (rc)
                break;
        }
        if (rc != 0 && rc != MDB_NOTFOUND)
            goto cpaper_finalize;
        if (rc != MDB_NOTFOUND && mdb_cmp(db->txn,
                                          db->papers_dbi,
                                          &key_influential_references,
                                          &key_papers) == 0) {
            rc = mdb_cursor_count(influential_references_cursor,
                                  &influential_references_len);
            if (rc)
                goto cpaper_finalize;
        }

        while (mdb_cmp(db->txn,
                       db->papers_dbi,
                       &key_noninfluential_references,
                       &key_papers) < 0) {
            rc = mdb_cursor_get(noninfluential_references_cursor,
                                &key_noninfluential_references,
                                &data_noninfluential_references,
                                MDB_NEXT_NODUP);
            if (rc)
                break;
        }
        if (rc != 0 && rc != MDB_NOTFOUND)
            goto cpaper_finalize;
        if (rc != MDB_NOTFOUND && mdb_cmp(db->txn,
                                          db->papers_dbi,
                                          &key_noninfluential_references,
                                          &key_papers) == 0) {
            rc = mdb_cursor_count(noninfluential_references_cursor,
                                  &noninfluential_references_len);
            if (rc)
                goto cpaper_finalize;
        }

        all_citeref = (corpusid_t*)malloc(
          (influential_citations_len + noninfluential_citations_len +
           influential_references_len + noninfluential_references_len) *
          sizeof(corpusid_t));
        if (!all_citeref)
            abort();
        influential_citations = all_citeref;
        noninfluential_citations =
          influential_citations + influential_citations_len;
        influential_references =
          noninfluential_citations + noninfluential_citations_len;
        noninfluential_references =
          influential_references + influential_references_len;

        for (i = 0; i < influential_citations_len;) {
            influential_citations[i] =
              *(corpusid_t*)data_influential_citations.mv_data;
            i++;
            if (i < influential_citations_len) {
                rc = mdb_cursor_get(influential_citations_cursor,
                                    &key_influential_citations,
                                    &data_influential_citations,
                                    MDB_NEXT_DUP);
                if (rc) {
                    free(all_citeref);
                    goto cpaper_finalize;
                }
            }
        }

        for (i = 0; i < noninfluential_citations_len;) {
            noninfluential_citations[i] =
              *(corpusid_t*)data_noninfluential_citations.mv_data;
            i++;
            if (i < noninfluential_citations_len) {
                rc = mdb_cursor_get(noninfluential_citations_cursor,
                                    &key_noninfluential_citations,
                                    &data_noninfluential_citations,
                                    MDB_NEXT_DUP);
                if (rc) {
                    free(all_citeref);
                    goto cpaper_finalize;
                }
            }
        }

        for (i = 0; i < influential_references_len;) {
            influential_references[i] =
              *(corpusid_t*)data_influential_references.mv_data;
            i++;
            if (i < influential_references_len) {
                rc = mdb_cursor_get(influential_references_cursor,
                                    &key_influential_references,
                                    &data_influential_references,
                                    MDB_NEXT_DUP);
                if (rc) {
                    free(all_citeref);
                    goto cpaper_finalize;
                }
            }
        }

        for (i = 0; i < noninfluential_references_len;) {
            noninfluential_references[i] =
              *(corpusid_t*)data_noninfluential_references.mv_data;
            i++;
            if (i < noninfluential_references_len) {
                rc = mdb_cursor_get(noninfluential_references_cursor,
                                    &key_noninfluential_references,
                                    &data_noninfluential_references,
                                    MDB_NEXT_DUP);
                if (rc) {
                    free(all_citeref);
                    goto cpaper_finalize;
                }
            }
        }

        data_mdb_val_to_paper(&data_papers, &paper);
        data_mdb_val_to_abstract(&data_abstracts, &abstract);
        data_mdb_val_to_tldr(&data_tldrs, &tldr);

        data_create_cpaper(
          &cpaper,
          &paper,
          (has_id ? (sha1_t*)data_paper_ids.mv_data : &invalid_sha1),
          (has_abstract ? abstract.abstract : NULL),
          (has_abstract ? abstract.abstractlen : 0),
          (has_tldr ? tldr.tldr : NULL),
          (has_tldr ? tldr.tldrlen : 0),
          influential_citations,
          influential_citations_len,
          noninfluential_citations,
          noninfluential_citations_len,
          influential_references,
          influential_references_len,
          noninfluential_references,
          noninfluential_references_len);
        free(all_citeref);
        data_cpaper_to_mdb_val(&cpaper, &data_cpapers);
        rc = mdb_put(
          cdb->txn, cdb->papers_dbi, &key_papers, &data_cpapers, MDB_APPEND);
        data_free_cpaper(&cpaper);

        if (rc)
            goto cpaper_finalize;

        rc = mdb_cursor_get(papers_cursor, &key_papers, &data_papers, MDB_NEXT);
        if (rc == MDB_NOTFOUND) {
            rc = 0;
            break;
        }
        if (rc)
            goto cpaper_finalize;
    }


    fprintf(stderr, "\r inserted  %ld it\n", iter);

cpaper_finalize:
    mdb_cursor_close(papers_cursor);
    mdb_cursor_close(paper_ids_cursor);
    mdb_cursor_close(abstracts_cursor);
    mdb_cursor_close(tldrs_cursor);
    mdb_cursor_close(influential_citations_cursor);
    mdb_cursor_close(noninfluential_citations_cursor);
    mdb_cursor_close(influential_references_cursor);
    mdb_cursor_close(noninfluential_references_cursor);

    return rc;
}

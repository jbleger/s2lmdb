#ifndef _HEADER_DB
#define _HEADER_DB 1

#include "data_manip.h"
#include <stdio.h>

#define MAXIMUM_LMDB_SIZE 1099511627776L

#define MAX_ENTRIES_ABSTRACTS_CST 50000
#define MAX_ENTRIES_TLDRS_CST 200000
#define MAX_ENTRIES_IDS_CST 500000
#define MAX_ENTRIES_PAPERS_CST 200000
#define MAX_ENTRIES_AUTHORS_CST 1000000
#define MAX_ENTRIES_AUTHORSCITREFS_CST 1000000
#define MAX_ENTRIES 20000000

struct db_t
{
    MDB_env* env;
    MDB_txn* txn;
    MDB_dbi assoc_dbi;
    uint64_t counter;
    uint64_t ramGB;
    MDB_dbi papers_dbi;
    MDB_dbi paper_ids_by_corpusid_dbi;
    MDB_dbi paper_ids_by_sha1_dbi;
    MDB_dbi abstracts_dbi;
    MDB_dbi tldrs_dbi;
    MDB_dbi authorships_by_authorid_dbi;
    MDB_dbi authors_dbi;
    MDB_dbi influential_citations_dbi;
    MDB_dbi noninfluential_citations_dbi;
    MDB_dbi influential_references_dbi;
    MDB_dbi noninfluential_references_dbi;
    uint64_t papers_counts;
    uint64_t paper_ids_by_corpusid_counts;
    uint64_t paper_ids_by_sha1_counts;
    uint64_t abstracts_counts;
    uint64_t tldrs_counts;
    uint64_t authorships_by_authorid_counts;
    uint64_t authors_counts;
    uint64_t influential_citations_counts;
    uint64_t noninfluential_citations_counts;
    uint64_t influential_references_counts;
    uint64_t noninfluential_references_counts;
};

typedef struct db_t db_t;

int
db_open_database(db_t* db, char* pathname, uint64_t ramGB, int rw);

int
db_commit_database(db_t* db);

void
db_close_database(db_t* db);

int
db_renew_dbi(db_t* db,
             char* name,
             MDB_dbi* dbi,
             uint64_t* counter,
             unsigned int flags);

int
db_new_dbi(db_t* db,
           char* name,
           MDB_dbi* dbi,
           uint64_t* counter,
           unsigned int flags);

int
db_renew_all_dbi_if_needed(db_t* db);

#endif

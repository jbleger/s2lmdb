#ifndef _HEADER_CDB
#define _HEADER_CDB 1

#include "data_manip.h"
#include <stdio.h>

#define MAXIMUM_LMDB_SIZE 1099511627776L

struct cdb_t
{
    MDB_env* env;
    MDB_txn* txn;
    MDB_dbi papers_dbi;
    MDB_dbi paper_ids_dbi;
    MDB_dbi authors_dbi;
};

typedef struct cdb_t cdb_t;

int
cdb_open_database(cdb_t* db, char* pathname, int rw);

int
cdb_abort_database(cdb_t* db);

int
cdb_commit_database(cdb_t* db);

void
cdb_close_database(cdb_t* db);

typedef MDB_cursor* cdb_papers_cursor_t;

typedef MDB_cursor* cdb_authors_cursor_t;

#endif

#ifndef _HEADER_CDB_QUERY
#define _HEADER_CDB_QUERY

#include "cdb.h"
#include "data.h"

int
cdb_create_authors_cursor(cdb_t* cdb, cdb_authors_cursor_t* cursor);

int
cdb_authors_cursor_get(cdb_authors_cursor_t* cursor, authorid_t* authorid, cauthor_t* cauthor, unsigned int what);

int
cdb_get_author(cdb_t* cdb, authorid_t authorid, cauthor_t* cauthor);

void
cdb_close_authors_cursor(cdb_papers_cursor_t* cursor);

int
cdb_get_authorslen(cdb_t* cdb, uint64_t *authorslen);

int
cdb_get_paper(cdb_t* cdb, corpusid_t corpusid, cpaper_t* cpaper);

int
cdb_get_corpusid_from_sha1(cdb_t* cdb, sha1_t* sha1, corpusid_t *corpusid);

int
cdb_get_paperslen(cdb_t* cdb, uint64_t *paperslen);

int
cdb_create_papers_cursor(cdb_t* cdb, cdb_papers_cursor_t* cursor);

void
cdb_close_papers_cursor(cdb_papers_cursor_t* cursor);

int
cdb_papers_cursor_get(cdb_papers_cursor_t* cursor, corpusid_t* corpusid, cpaper_t* cpaper, unsigned int what);

#endif

#include "cdb.h"

int
cdb_open_database(cdb_t* cdb, char* pathname, int rw)
{
    int rc;

    rc = mdb_env_create(&cdb->env);
    if (rc)
        return (rc);

    rc = mdb_env_set_mapsize(cdb->env, MAXIMUM_LMDB_SIZE);
    if (rc)
        return (rc);

    rc = mdb_env_set_maxdbs(cdb->env, 4);
    if (rc)
        return (rc);

    rc =
      mdb_env_open(cdb->env,
                   pathname,
                   MDB_NOSUBDIR | MDB_NORDAHEAD | MDB_NOMETASYNC | MDB_NOLOCK,
                   0644);
    if (rc)
        return (rc);

    if (rw)
        rc = mdb_txn_begin(cdb->env, NULL, 0, &cdb->txn);
    else
        rc = mdb_txn_begin(cdb->env, NULL, MDB_RDONLY, &cdb->txn);
    if (rc)
        return (rc);

    unsigned int common_flags = 0;
    if (rw)
        common_flags |= MDB_CREATE;

    rc = mdb_dbi_open(
      cdb->txn, "papers", common_flags | MDB_INTEGERKEY, &cdb->papers_dbi);
    if (rc)
        return (rc);

    rc = mdb_dbi_open(cdb->txn, "paper_ids", common_flags, &cdb->paper_ids_dbi);
    if (rc)
        return (rc);

    rc = mdb_dbi_open(
      cdb->txn, "authors", common_flags | MDB_INTEGERKEY, &cdb->authors_dbi);
    if (rc)
        return (rc);

    return 0;
}

int
cdb_commit_database(cdb_t* cdb)
{

    int rc;
    rc = mdb_txn_commit(cdb->txn);
    if (rc)
        return rc;
    rc = mdb_txn_begin(cdb->env, NULL, 0, &cdb->txn);
    return rc;
}

int
cdb_abort_database(cdb_t* cdb)
{
    int rc;
    mdb_txn_abort(cdb->txn);
    rc = mdb_txn_begin(cdb->env, NULL, 0, &cdb->txn);
    return rc;
}

void
cdb_close_database(cdb_t* db)
{
    mdb_txn_abort(db->txn);
    mdb_close(db->env, db->papers_dbi);
    mdb_env_close(db->env);
}

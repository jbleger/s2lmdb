#include "cdb_query.h"

int
cdb_get_author(cdb_t* cdb, authorid_t authorid, cauthor_t* cauthor)
{
    int rc;
    MDB_val key, data;
    data_authorid_to_mdb_val(&authorid, &key);
    rc = mdb_get(cdb->txn, cdb->authors_dbi, &key, &data);
    if (rc)
        return rc;
    data_mdb_val_to_cauthor(&data, cauthor);
    return 0;
}

int
cdb_get_authorslen(cdb_t* cdb, uint64_t *authorslen)
{
    int rc;
    MDB_stat stats;
    rc = mdb_stat(cdb->txn, cdb->authors_dbi, &stats);
    if(rc)
        return rc;
    *authorslen = stats.ms_entries;
    return 0;
}

int
cdb_create_authors_cursor(cdb_t* cdb, cdb_authors_cursor_t* cursor)
{
    int rc;
    rc = mdb_cursor_open(cdb->txn, cdb->authors_dbi, cursor);
    return rc;
}

int
cdb_authors_cursor_get(cdb_authors_cursor_t* cursor, authorid_t* authorid, cauthor_t* cauthor, unsigned int what)
{
    int rc;
    MDB_val key, data;
    rc = mdb_cursor_get(*cursor, &key, &data, what);
    if (rc)
        return rc;
    *authorid = *((authorid_t*)key.mv_data);
    data_mdb_val_to_cauthor(&data, cauthor);
    return 0;
}

void
cdb_close_authors_cursor(cdb_papers_cursor_t* cursor)
{
    mdb_cursor_close(*cursor);
}

int
cdb_get_paper(cdb_t* cdb, corpusid_t corpusid, cpaper_t* cpaper)
{
    int rc;
    MDB_val key, data;
    data_corpusid_to_mdb_val(&corpusid, &key);
    rc = mdb_get(cdb->txn, cdb->papers_dbi, &key, &data);
    if (rc)
        return rc;
    data_mdb_val_to_cpaper(&data, cpaper);
    return 0;
}

int
cdb_get_corpusid_from_sha1(cdb_t* cdb, sha1_t* sha1, corpusid_t *corpusid)
{
    int rc;
    MDB_val key, data;
    data_sha1_to_mdb_val(sha1, &key);
    rc = mdb_get(cdb->txn, cdb->paper_ids_dbi, &key, &data);
    if (rc)
        return rc;
    *corpusid = *((corpusid_t*)data.mv_data);
    return 0;
}

int
cdb_get_paperslen(cdb_t* cdb, uint64_t *paperslen)
{
    int rc;
    MDB_stat stats;
    rc = mdb_stat(cdb->txn, cdb->papers_dbi, &stats);
    if(rc)
        return rc;
    *paperslen = stats.ms_entries;
    return 0;
}

int
cdb_create_papers_cursor(cdb_t* cdb, cdb_papers_cursor_t* cursor)
{
    int rc;
    rc = mdb_cursor_open(cdb->txn, cdb->papers_dbi, cursor);
    return rc;
}

void
cdb_close_papers_cursor(cdb_papers_cursor_t* cursor)
{
    mdb_cursor_close(*cursor);
}

int
cdb_papers_cursor_get(cdb_papers_cursor_t* cursor, corpusid_t* corpusid, cpaper_t* cpaper, unsigned int what)
{
    int rc;
    MDB_val key, data;
    rc = mdb_cursor_get(*cursor, &key, &data, what);
    if (rc)
        return rc;
    *corpusid = *((corpusid_t*)key.mv_data);
    data_mdb_val_to_cpaper(&data, cpaper);
    return 0;
}

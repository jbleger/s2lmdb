#pylint: disable=missing-module-docstring
import collections
from ._internals import DB #pylint: disable=no-name-in-module

_allowed_hexa = frozenset('0123456789abcdefABCDEF')

class Iterable: #pylint: disable=too-few-public-methods
    def __init__(self, genfact):
        self._genfact = genfact

    def __iter__(self):
        return self._genfact()

class SizedIterable(Iterable): #pylint: disable=too-few-public-methods
    def __init__(self, genfact, length):
        super().__init__(genfact)
        self._len = length

    def __len__(self):
        return self._len

class Collection(SizedIterable): #pylint: disable=too-few-public-methods
    def __init__(self, genfact, length, contains):
        super().__init__(genfact, length)
        self._contains = contains

    def __contains__(self, key):
        return self._contains(key)

class PapersDict:
    """
    Read-only mapping interface to all papers.

    Usual operations on mapping are allowed:
      - getting with index:
        - `papers[1234]`
        - `papers[(1234, 4561)]`
      - iterating on keys, values, or items
    """
    _db: DB
    _len: int = None

    def __init__(self, db):
        self._db = db

    def __len__(self):
        if self._len is None:
            self._len = self._db.paperslen()
        return self._len

    def __iter__(self):
        return (k for k,_ in self._db.get_all_papers())

    def __getitem__(self, key):
        if isinstance(key, (int,str)):
            return self._get_one_item(key)
        if isinstance(key, collections.abc.Iterable):
            return self._db.get_many_papers(list(key), raisekeyerror=True)
        raise TypeError(
                "Key must be a integer, a sha1 hex string, or a iterable of "
                f"integers, found: {key!r}"
                )

    def _get_one_item(self, key):
        if isinstance(key, int):
            return self._db.get_paper_by_corpusid(key)
        if isinstance(key, str) and len(key)==40 and all(x in _allowed_hexa for
                x in key):
            return self._db.get_paper_by_sha1(key)
        return TypeError(f'keys must be integers or sha1 hex strings, found {key!r}')

    def _get_one_item_or_default(self, key, default=None):
        try:
            return self._get_one_item(key)
        except KeyError:
            return default

    def __contains__(self, key):
        if isinstance(key, int):
            try:
                self._db.get_paper_by_corpusid(key)
            except KeyError:
                return False
            return True
        if isinstance(key, str) and len(key)==40 and all(x in _allowed_hexa for
                x in key):
            try:
                self._db.get_paper_by_sha1(key)
            except KeyError:
                return False
            return True
        return False

    def get(self, key, default=None):
        if isinstance(key, (int,str)):
            return self._get_one_item_or_default(key, default=default)
        if isinstance(key, collections.abc.Iterable):
            return self._db.get_many_papers(list(key), raisekeyerror=False, default=default)
        raise TypeError(
                "Key must be a integer, a sha1 hex string, or a iterable of "
                f"integers, found: {key!r}"
                )

    def keys(self):
        return Collection(
                genfact=lambda: (k for k,_ in self._db.get_all_papers()),
                length = len(self),
                contains = self.__contains__)

    def values(self):
        return SizedIterable(
                genfact=lambda: (v for _,v in self._db.get_all_papers()),
                length = len(self))

    def items(self):
        return SizedIterable(
                genfact=self._db.get_all_papers,
                length = len(self))


class AuthorsDict:
    """
    Read-only mapping interface to all authors.

    Usual operations on mapping are allowed:
      - getting with index:
        - `authors[5166219]`
        - `authors[(48387599, 3107429, 2154309850)]`
      - iterating on keys, values, or items
    """
    _db: DB
    _len: int = None

    def __init__(self, db):
        self._db = db

    def __len__(self):
        if self._len is None:
            self._len = self._db.authorslen()
        return self._len

    def __iter__(self):
        return (k for k,_ in self._db.get_all_authors())

    def __getitem__(self, key):
        if isinstance(key, int):
            return self._get_one_item(key)
        if isinstance(key, collections.abc.Iterable):
            return self._db.get_many_authors(list(key), raisekeyerror=True)
        raise TypeError(
                "Key must be a integer, or a iterable of "
                f"integers, found: {key!r}"
                )

    def _get_one_item(self, key):
        if isinstance(key, int):
            return self._db.get_author_by_authorid(key)
        return TypeError(f'keys must be integers, found {key!r}')

    def _get_one_item_or_default(self, key, default=None):
        try:
            return self._get_one_item(key)
        except KeyError:
            return default

    def __contains__(self, key):
        if isinstance(key, int):
            try:
                self._db.get_author_by_authorid(key)
            except KeyError:
                return False
            return True
        return False

    def get(self, key, default=None):
        if isinstance(key, int):
            return self._get_one_item_or_default(key, default=default)
        if isinstance(key, collections.abc.Iterable):
            return self._db.get_many_authors(list(key), raisekeyerror=False, default=default)
        raise TypeError(
                "Key must be a integer, or a iterable of "
                f"integers, found: {key!r}"
                )


    def keys(self):
        return Collection(
                genfact=lambda: (k for k,_ in self._db.get_all_authors()),
                length = len(self),
                contains = self.__contains__)

    def values(self):
        return SizedIterable(
                genfact=lambda: (v for _,v in self._db.get_all_authors()),
                length = len(self))

    def items(self):
        return SizedIterable(
                genfact=self._db.get_all_authors,
                length = len(self))

class S2database: #pylint: disable=too-few-public-methods
    """Read-only interface to manululate S2 database.

    The interface provide the following mapping (accessible as read-only
    properties):
        - papers: interface to query papers
        - authors: interface to query authors
    """
    _db: DB
    _papersdict: PapersDict
    _authorsdict: AuthorsDict

    def __init__(self, pathname):
        """Read-only interface to manululate S2 database.

        Parameters:
        -----------
        filename : str
            filename of the database. The database must be build before.
        """

        self._db = DB(pathname)

        self._papersdict = PapersDict(self._db)
        self._authorsdict = AuthorsDict(self._db)

    @property
    def papers(self):
        """Read-only interface to all papers"""
        return self._papersdict

    @property
    def authors(self):
        """Read-only interface to all papers"""
        return self._authorsdict

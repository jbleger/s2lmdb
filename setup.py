"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

from os import path
from setuptools import setup, find_packages

from setuptools.extension import Extension
from Cython.Build import cythonize

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "s2lmdb", "__init__.py"), encoding="utf-8") as f:
    ast = compile(f.read(), "__init__.py", "exec")
    fake_global = {"__name__": "__main__"}
    try:
        exec(ast, fake_global)
    except (SystemError, ImportError) as e:
        print("System error")

    long_description = "Description"  # fake_global["__doc__"]
    version = fake_global["__version__"]

extensions = [
    Extension(
        "s2lmdb._internals",
        ["s2lmdb/_internals.pyx"],
        extra_compile_args=["-fopenmp"],
        extra_link_args=["-fopenmp", "-llmdb"],
    ),
]


setup(
    name="s2lmdb",
    version=version,
    description="S2 with lmdb",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Jean-Benoist Leger",
    author_email="jbleger@hds.utc.fr",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT Livence",
        "Programming Language :: Python :: 3 :: Only",
    ],
    url="https://example.com/",
    packages=find_packages(exclude=["contrib", "docs", "tests", "src"]),
    install_requires=(),
    extras_require={"test": ["pytest"]},
    package_data={},
    data_files=[],
    python_requires=">=3.9",
    entry_points={},
    project_urls={},
    ext_modules=cythonize(extensions),
)
